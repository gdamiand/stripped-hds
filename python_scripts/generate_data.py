import argparse
import itertools
import os
import subprocess
import sys
import time

from enum import Enum

RANDOM_SEGMENTS_BIN = "../src/builds/release/tools/random-segments"
SHP_TO_TXT_BIN =      "../src/builds/release/tools/shp-to-txt"

class Profile(Enum):

    GAUSS = "gauss"
    RAND  = "rand"
    BEST  = "best"
    WORST = "worst"

    def __str__(self):
        return self.value

    def __repr__(self):
        return str(self)

def bin_exist(path):

    return os.path.isfile(path) and os.access(path, os.X_OK)

def random_segments(xmin=0.0, xmax=10000.0, ymin=0.0, ymax=10000.0,
                    n=100, size=1000.0, error=0.05,
                    output="./segments.txt",
                    seed=None, fixed=False):

    args = [
        RANDOM_SEGMENTS_BIN,
        "--xmin",       str(xmin),
        "--xmax",       str(xmax),
        "--ymin",       str(ymin),
        "--ymax",       str(ymax),
        "-n",           str(n),
        "--size",       str(size),
        "--error",      str(error),
        "--output",     output
    ]
    if seed:
        args.append("--seed")
        args.append(str(seed))
    if fixed:
        args.append("--fixed ")

    proc = subprocess.run(args, stdout=subprocess.PIPE)

    return proc

def shp_to_txt(profile, n,
               band=None, size=None,
               output=None):

    args = [
        SHP_TO_TXT_BIN
    ]

    if profile == Profile.RAND:

        args.extend([ "-t2", str(n), str(size) ])
        if size == 0.0:
            filename = "random-%d.txt" % n
        else:
            filename = "random-%d-size-%.2f.txt" % (n, size)

    elif profile == Profile.BEST:

        args.extend([ "-t5", str(n), str(band), str(size) ])
        if band == 1:
            if size == 0.0:
                filename = "random-%d.txt" % n
            else:
                filename = "random-%d-size-%.2f.txt" % (n, size)
        else:
            if size == 0.0:
                filename = "random-%d-best-%d.txt" % (n, band)
            else:
                filename = "random-%d-size-%.2f-best-%d.txt" % (n, size, band)

    elif profile == Profile.WORST:

        args.extend([ "-t6", str(n) ])
        filename = "random-%d-worst.txt" % (n)

    else:
        raise NameError("'" + profile + "' is not a valid profile for shp-to-txt.")

    proc = subprocess.run(args, stdout=subprocess.PIPE)

    if output:
        os.rename(filename, output)

    return proc

if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument("--profile", "-p", required=True, type=Profile,
                        choices=list(Profile),
                        help="set generation profile")

    parser.add_argument("--nseg", "-n", required=True, type=int, nargs="+",
                        help="set number of segments")
    parser.add_argument("--size", "-s", required=True, type=float, nargs="+",
                        help="set size of segments")
    parser.add_argument("--batch", "-b", required=True, type=int,
                        help="set number of segments generations")

    parser.add_argument('--verbose', '-v', action='count', default=0,
                        help="set verbosity level")
    parser.add_argument("--dir", "-d", default=None,
                        help="set generated data directory, default is cwd")

    gaussg = parser.add_argument_group("gauss profile configuration")

    gaussg.add_argument("--error", type=float, default=0.10,
                        help="set segment length error (=2*sigma/mu), default value is 0.10")
    gaussg.add_argument("--fixed", action="store_true",
                        help="fix segment size")

    bestgr = parser.add_argument_group("best profile configuration")

    bestgr.add_argument("--band", type=int, nargs="+",
                        required=(str(Profile.BEST) in sys.argv),
                        help="set numbers of bands")

    binary = parser.add_argument_group("binaries configuration")

    binary.add_argument("--bin-gauss", default=RANDOM_SEGMENTS_BIN,
                        help="set 'random-segments' binary path, default value is set line 10")
    binary.add_argument("--bin-shp", default=SHP_TO_TXT_BIN,
                        help="set 'shp-to-txt' binary path, default value is set line 11")

    args = parser.parse_args()

    RANDOM_SEGMENTS_BIN = args.bin_gauss
    SHP_TO_TXT_BIN = args.bin_shp

    if args.dir == None:
        args.dir = os.getcwd()

    if not bin_exist(RANDOM_SEGMENTS_BIN):
        raise NameError("'" + RANDOM_SEGMENTS_BIN + "' random-segments path does not exist. "
                        "Set variable 'RANDOM_SEGMENTS_BIN' line 10 or option --bin-gauss to a correct path.")
    if not bin_exist(SHP_TO_TXT_BIN):
        raise NameError("'" + SHP_TO_TXT_BIN + "' shp-to-txt path does not exist. "
                        "Set variable 'SHP_TO_TXT_BIN' line 11 or option --bin-shp to a correct path.")

    if not os.path.isdir(args.dir):
        raise NameError("'" + args.dir + "' directory does not exist.")

    # Si le mode est 'worst' alors la taille des segments est inutiles.
    if args.profile == Profile.WORST:
        args.size = [0]

    iter = 0
    total_iter = len(args.nseg) * len(args.size) * args.batch
    if args.profile == Profile.BEST:
        total_iter *= len(args.band)

    def advance_and_print():

        global iter
        iter += 1
        if args.verbose > 1:
            print("progress: %d / %d" % (iter, total_iter))

    def format_path(nseg, size, i, band=None):

        filename = "data-%s-%d" % (args.profile, nseg)
        if args.profile != Profile.WORST:
            filename += "-size-%d" % size
        if args.profile == Profile.BEST and band != None:
            filename += "-band-%d" % band
        filename += "-%d.txt" % i

        return os.path.join(args.dir, filename)

    start = time.time()

    for nseg, size, i in itertools.product(args.nseg, args.size, range(args.batch)):

        if args.profile == Profile.BEST:

            for band in args.band:
                shp_to_txt(Profile.BEST, nseg, size=size, band=band,
                           output=format_path(nseg, size, i, band=band))
                advance_and_print()

        elif args.profile == Profile.GAUSS:

            random_segments(n=nseg, size=size, error=args.error, fixed=args.fixed,
                            output=format_path(nseg, size, i))
            advance_and_print()

        else:

            shp_to_txt(args.profile, nseg, size=size,
                       output=format_path(nseg, size, i))
            advance_and_print()

    stop = time.time()
    if args.verbose:
        print("ellapsed time:", stop - start, "sec")
