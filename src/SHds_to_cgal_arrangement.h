 // compute_arrangement: a new method to compute arrangement of segments.
// Copyright (C) 2020 CNRS and LIRIS' Establishments (France).
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
// Author(s)     : Guillaume Damiand <guillaume.damiand@liris.cnrs.fr>
//

#ifndef SHDS_TO_CGAL_ARRANGEMENT_H
#define SHDS_TO_CGAL_ARRANGEMENT_H

#include "SHds.h"
#include <CGAL/Arrangement_2.h>

// Build a CGAL arrangement from a parallel arrangement.
template<typename Arr>
void shds_to_cgal_arrangement(SHds& shds, Arr& arr)
{
  arr.clear();
  std::vector<ESegment> all_segments;
  all_segments.reserve(shds.number_of_non_external_halfedges()/2);

  Global_halfedge hh;
  for (std::size_t i=0; i<shds.number_of_strips(); ++i)
  {
    for (auto it=shds.hds(i).halfedges().begin(),
         itend=shds.hds(i).halfedges().end(); it!=itend; ++it)
    {
      if (!shds.partial_hds(i).is_external(it))
      { // First halfedge is it
        hh=shds.opposite(Global_halfedge(i, it)); // Opposite halfedge, a non critical dart
        if (it<hh.halfedge()) // To add a segment only once
        { all_segments.push_back(ESegment(shds.hds(i).point(it),
                                          shds.hds(hh).point(hh.halfedge()))); }
      }
    }
  }

  CGAL::insert_non_intersecting_curves(arr,
                                       all_segments.begin(), all_segments.end());
}

#endif // SHDS_TO_CGAL_ARRANGEMENT_H
