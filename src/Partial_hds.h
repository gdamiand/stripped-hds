// compute_arrangement: a new method to compute arrangement of segments.
// Copyright (C) 2020 CNRS and LIRIS' Establishments (France).
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
// Author(s)     : Guillaume Damiand <guillaume.damiand@liris.cnrs.fr>
//

#ifndef PARTIAL_HDS_H
#define PARTIAL_HDS_H

#include <iostream>
#include <thread>
#include <vector>
#include <tuple>
#include <stack>
#include <map>

#include "Print_txt.h"
#include "Hds.h"
#include "Export_xfig.hh"

template <typename GeometryTraits_2, typename Allocator_>
class My_visitor;
template <typename Kernel_>
class My_arr_segment_traits_2;
class SHds;

////////////////////////////////////////////////////////////////////////////////
class Partial_hds
{
public:
  using Self=Partial_hds;

  template <typename GeometryTraits_2, typename Allocator_>
  friend class My_visitor;

  friend class SHds;
  friend class MySimpleSHDSViewerQt;

  Partial_hds(double x1=0., double y1=0., double x2=0., double y2=0.,
              std::size_t strip_id=0, std::size_t nb_strips=0):
    m_minx(x1),
    m_maxx(x2),
    m_min(x1, y1),
    m_max(x2, y2),
    m_strip_id(strip_id),
    m_nb_strips(nb_strips),
    m_nb_input_segments(0)
  {
    /*print_txt_with_endl("[Strip ", m_strip_id, "]: ", x1, ", ",
                        y1, ", ", x2, ", ", y2); */
  }

  std::size_t strip_id() const
  { return m_strip_id; }
  std::size_t number_of_strips() const
  { return m_nb_strips; }

  ECoord xmin() const
  { return m_min.x(); }
  ECoord ymin() const
  { return m_min.y(); }
  ECoord xmax() const
  { return m_max.x(); }
  ECoord ymax() const
  { return m_max.y(); }

  const EPoint& pmin() const
  { return m_min; }
  const EPoint& pmax() const
  { return m_max; }

  HDS& hds()
  { return m_hds; }
  const HDS& hds() const
  { return m_hds; }

  std::size_t number_of_left_externals() const
  { return m_left_external_halfedges.size(); }
  std::size_t number_of_right_externals() const
  { return m_right_external_halfedges.size(); }
  std::size_t number_of_externals() const
  { return number_of_left_externals()+number_of_right_externals(); }
  std::size_t number_of_non_externals() const
  { return hds().number_of_halfedges()-number_of_externals(); }
  std::size_t number_of_input_segments() const
  { return m_nb_input_segments; }
  std::size_t number_of_vertices() const
  { return hds().number_of_vertices(); }

  void clear()
  {
    m_minx=m_maxx=0;
    m_min=m_max=CGAL::ORIGIN;
    m_strip_id=m_nb_strips=0;
    m_hds.clear();
    m_left_external_halfedges.clear();
    m_right_external_halfedges.clear();
  }

  bool point_in_strip(const EPoint& p) const
  {
    // in strip means p.x >= m_min.x && p.x < m_max.x
    return (CGAL::compare_x(p, m_min)!=CGAL::SMALLER &&
            CGAL::compare_x(p, m_max)==CGAL::SMALLER);
  }

  void add_left_external_halfedge(Halfedge_handle hh)
  {
    /*print_txt_with_endl("[strip ", strip_id(), "] add_left_external_halfedge ",
                        hds().edge_id(hh), " ptr=", &*hh);*/

    assert(is_left_external(hh));
    assert(m_left_external_halfedges.count(hds().edge_id(hh))==0);
    assert(hds().left_to_right(hh));
    m_left_external_halfedges.insert(std::make_pair(hds().edge_id(hh), hh));
  }

  void add_right_external_halfedge(Halfedge_handle hh)
  {
    /*print_txt_with_endl("[strip ", strip_id(), "] add_right_external_halfedge ",
                        -hds().edge_id(hh), " ptr=", &*hh);*/

    assert(is_right_external(hh));
    assert(m_right_external_halfedges.count(-hds().edge_id(hh))==0);
    assert(hds().right_to_left(hh));

    m_right_external_halfedges.insert(std::make_pair(-hds().edge_id(hh), hh));
  }

  void set_above_halfedge(Vertex_handle vh, Halfedge_handle hh)
  { vh->set_above_halfedge(hh); }

  // @return true iff hh is external, i.e. not linked with an inner point in
  // this strip.
  bool is_external(Halfedge_handle hh) const
  { return hds().vertex(hh)==nullptr; }

  // @return true iff hh is left external, i.e. external and intersect the
  // left side of the strip (and thus from left to right).
  bool is_left_external(Halfedge_handle hh) const
  { return is_external(hh) && hds().left_to_right(hh); }

  // @return true iff hh is right external, i.e. external and intersect the
  // right side of the strip (and thus from right to left).
  bool is_right_external(Halfedge_handle hh) const
  { return is_external(hh) && hds().right_to_left(hh); }

  bool source_in_strip(Halfedge_handle hh) const
  { return !is_external(hh); }
  bool target_in_strip(Halfedge_handle hh) const
  { return !is_external(hds().opposite(hh)); }

  bool is_valid_external_halfedges()
  {
    bool res=true;

    if (m_strip_id==0 && !m_left_external_halfedges.empty())
    {
      print_txt_with_endl("[strip 0] : ERROR m_left_external_halfedges "
                          "is not empty.");
    }
    if (m_strip_id==m_nb_strips-1 && !m_right_external_halfedges.empty())
    {
      print_txt_with_endl("[strip ", strip_id(),
                          "] (last strip) : ERROR, m_right_external_halfedges "
                          "is not empty.");
    }

    for (auto it=hds().halfedges().begin(), itend=hds().halfedges().end();
         it!=itend; ++it)
    {
      if (is_left_external(it))
      {
        auto resfind=m_left_external_halfedges.find(hds().edge_id(it));
        if (resfind==m_left_external_halfedges.end())
        { print_txt_with_endl("[strip ", strip_id(),
                              "] : ERROR, halfedge ",
                              hds().edge_id(it), " is left external but ",
                              "is not in array m_left_external_halfedges.");
          res=false;
        }
        else
        {
          if (resfind->second!=it)
          { print_txt_with_endl("[strip ", strip_id(),
                                "] : ERROR, halfedge ",
                                hds().edge_id(it), " is left external but ",
                                "its associated halfedge in array ",
                                "m_left_external_halfedges is different ",
                                hds().edge_id(resfind->second),
                                " PTR ", &*(resfind->second), " AND ",
                                &*(it));
            res=false;
          }
        }
      }
      if (is_right_external(it))
      {
        auto resfind=m_right_external_halfedges.find(-hds().edge_id(it));
        if (resfind==m_right_external_halfedges.end())
        { print_txt_with_endl("[strip ", strip_id(),
                              "] : ERROR, halfedge ",
                              hds().edge_id(it), " is right external but ",
                              "is not in array m_right_external_halfedges.");
          res=false;
        }
        else
        {
          if (resfind->second!=it)
          { print_txt_with_endl("[strip ", strip_id(),
                                "] : ERROR, halfedge ",
                                hds().edge_id(it), " is right external but ",
                                "its associated halfedge in array ",
                                "m_right_external_halfedges is different ",
                                hds().edge_id(resfind->second));
            res=false;
          }
        }
      }
    }
    return res;
  }

  friend std::ostream& operator<<(std::ostream& os,
                                  const Partial_hds& la)
  {
    CGAL::exact(la.m_min); CGAL::exact(la.m_max);
    os<<std::setprecision(17)
     <<la.m_minx<<" "<<la.m_maxx<<" "<<la.m_min<<" "<<la.m_max<<" "
     <<la.m_strip_id<<" "<<la.m_nb_strips<<" "<<la.m_nb_input_segments
    <<std::endl<<std::endl;
    os<<la.m_hds<<std::endl;

    os<<la.m_left_external_halfedges.size()<<" "
     <<la.m_right_external_halfedges.size()<<std::endl;

    for (auto it=la.m_left_external_halfedges.begin(),
         itend=la.m_left_external_halfedges.end(); it!=itend; ++it)
    { os<<it->first<<" "<<la.hds().halfedges().index(it->second)<<std::endl; }
    for (auto it=la.m_right_external_halfedges.begin(),
         itend=la.m_right_external_halfedges.end(); it!=itend; ++it)
    { os<<it->first<<" "<<la.hds().halfedges().index(it->second)<<std::endl; }

    return os;
  }

  friend std::istream& operator>>(std::istream& is,
                                  Partial_hds& la)
  {
    la.clear();

    is>>la.m_minx>>la.m_maxx>>la.m_min>>la.m_max
        >>la.m_strip_id>>la.m_nb_strips>>la.m_nb_input_segments;

    std::vector<Halfedge_handle> halfedges;
    std::vector<Vertex_handle> vertices;
    la.m_hds.load(is, halfedges, vertices);

    std::size_t nb_left, nb_right;
    std::size_t ind1;
    int int1;

    is>>nb_left>>nb_right;
    for (std::size_t i=0; i<nb_left; ++i)
    {
      is>>int1>>ind1;
      la.m_left_external_halfedges[int1]=halfedges[ind1];
    }
    for (std::size_t i=0; i<nb_right; ++i)
    {
      is>>int1>>ind1;
      la.m_right_external_halfedges[int1]=halfedges[ind1];
    }
    return is;
  }

  void display_info() const
  {
    print_txt_with_endl("[strip ", m_strip_id, "]: (", m_minx, " -> ", m_maxx,
                        ") #input-segments ", m_nb_input_segments,
                        " #left-external=", m_left_external_halfedges.size(),
                        " #right-external=", m_right_external_halfedges.size(),
                        " #halfedges=", hds().number_of_halfedges(),
                        " #vertices=", hds().number_of_vertices());
  }

  void export_to_xfig(Export_xfig& export_xfig,
                      double dx, double dy,
                      uint8_t coul_segments,
                      uint8_t coul_disks,
                      uint8_t coul_borders,
                      uint16_t width_segments,
                      uint16_t radius_disks,
                      uint16_t width_borders) const
  {
    // Border of the strip
    if (width_borders>0)
    {
      export_xfig.rectangle(CGAL::to_double(m_min.x())+dx,
                            CGAL::to_double(m_min.y())+dy,
                            CGAL::to_double(m_max.x())+dx,
                            CGAL::to_double(m_max.y())+dy,
                            coul_borders, width_borders, false, 200);
    }

    // HDS
    hds().export_to_xfig(export_xfig, dx, dy,
                         coul_segments, coul_disks,
                         width_segments, radius_disks);
  }

protected:
  double m_minx, m_maxx;
  EPoint m_min, m_max;
  std::size_t m_strip_id;
  std::size_t m_nb_strips;
  std::size_t m_nb_input_segments;

  HDS m_hds;
  std::unordered_map<int, Halfedge_handle> m_left_external_halfedges;
  std::unordered_map<int, Halfedge_handle> m_right_external_halfedges;
};
////////////////////////////////////////////////////////////////////////////////

#endif // PARTIAL_HDS_H
