// compute_arrangement: a new method to compute arrangement of segments.
// Copyright (C) 2020 CNRS and LIRIS' Establishments (France).
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
// Author(s)     : Guillaume Damiand <guillaume.damiand@liris.cnrs.fr>
//

#ifndef EXPORT_XFIG_H
#define EXPORT_XFIG_H

#include <cmath>
#include <string>
#include <fstream>

class Export_xfig
{
  constexpr static unsigned int MAX_PROF=999; // max depth for xfig

public:
  // xsize is the length of the bbox in x-coordinates
  // We resize the scene so that the x-length is xfig_x_size.
  Export_xfig(const std::string& filename, double xsize,
              double xfig_x_size=20000.,
              bool invert_y=false) :
    m_nb_couls(0),
    m_invert_y(invert_y),
    m_x_factor(xfig_x_size/xsize),
    m_x_shift(0),
    m_y_shift(0)
  {
    m_ofstream.open(filename);
    if (!m_ofstream)
    { std::cerr<<"ERROR opening "<<filename<<std::endl; }

    sauveEntete();
  }

  ~Export_xfig()
  { m_ofstream.close(); }

  void set_x_shift(int x)
  { m_x_shift=x; }
  void set_y_shift(int y)
  { m_y_shift=y; }

  uint8_t add_color(const CGAL::Color& c)
  {
    m_ofstream<<std::setfill('0');
    m_ofstream<<"0 "<<32+m_nb_couls<<" #"
             <<std::hex<<std::uppercase<<std::setw(2)
            <<static_cast<int>(c.red())
           <<static_cast<int>(c.green())
          <<static_cast<int>(c.blue())<<std::dec<<std::endl;
    ++m_nb_couls;
    return m_nb_couls-1;
  }

  /* TODO void debutComposante( ofstream &os,
                        const CVertex & min, const CVertex & max )
  {
    m_ofstream<<"6 "
     <<int(RINT(min.getX()+decalageX))<<" "
    <<int(RINT(min.getY()+decalageY))<<" "
    <<int(RINT(max.getX()+decalageX))<<" "
    <<int(RINT(max.getY()+decalageY))<<endl;
  }*/

  // Fin d'une composante : m_ofstream<<"-6\n";


  void point(double x, double y, uint16_t rayon,
             u_int8_t coul, uint16_t larg, uint16_t prof)
  {
    m_ofstream<<"1 4 0 "<<static_cast<int>(larg)<<" "
             <<static_cast<int>(32+coul)<<" "
            <<static_cast<int>(32+coul)<<" "
           <<static_cast<int>(prof);
    m_ofstream <<" 0 20 0.000 1 0.0000 ";


    m_ofstream<<point_to_string(x, y)<<" "  // Centre x et y
             <<static_cast<int>(rayon)<<" "<<static_cast<int>(rayon)<<" " //Rayon en x et en y
            <<point_to_string(x, y)<<" "  // Premier point == centre
           <<point_to_string(x+rayon, y)<<" " // deuxieme point (sur le cercle)
          <<std::endl;
  }

  void segment(double x1, double y1, double x2, double y2,
               u_int8_t coul, uint16_t larg, bool arrow, uint16_t prof)
  {
    int ix1=p_to_x(x1);
    int iy1=p_to_y(y1);
    int ix2=p_to_x(x2);
    int iy2=p_to_y(y2);

    if (ix1!=ix2 || iy1!=iy2)
    {
      m_ofstream<<"2 1 0 "<<static_cast<int>(larg)<<" "
               <<static_cast<int>(32+coul)<<" "
              <<static_cast<int>(32+coul)<<" "
             <<static_cast<int>(prof)
            <<" 0 -1 0.000 1 0 7 ";
      if (arrow)
      {
        m_ofstream<<"1 0 2\n"
                 <<"     2 0 1.00 60.00 120.00\n";
      }
      else
      { m_ofstream<<"0 0 2\n"; }

      m_ofstream<<ix1<<" "<<iy1<<" "<<ix2<<" "<<iy2<<std::endl;
    }
  }

  void rectangle(double x1, double y1, double x2, double y2,
                 u_int8_t coul, uint16_t larg, bool arrow, uint16_t prof)
  {
    segment(x1, y1, x2, y1, coul, larg, arrow, prof);
    segment(x2, y1, x2, y2, coul, larg, arrow, prof);
    segment(x2, y2, x1, y2, coul, larg, arrow, prof);
    segment(x1, y2, x1, y1, coul, larg, arrow, prof);
  }

protected:

  int transfoMmPixel(double val) const
  {
    // Xfig est en 1200 dpi, et donc le nombre de pixel par mm vaut 1200/25.4
    // (sachant que 1inch==25.4mm)
    // double dpmX=(1200/25.4);
    return std::round(val*m_x_factor); //(val*dpmX)/COEF_DE_REDUCTION);
  }

  int p_to_x(double val) const
  { return transfoMmPixel(val)+m_x_shift; }
  int p_to_y(double val) const
  {
    int res=transfoMmPixel(val)+m_y_shift;
    if (m_invert_y) { res=-res; }
    return res;
  }

  std::string value_to_string(double val)
  { return std::to_string(transfoMmPixel(val)); }

  std::string point_to_string(double x, double y)
  {
    int ix=p_to_x(x);
    int iy=p_to_y(y);
    return std::string(std::to_string(ix)+" "+std::to_string(iy));
  }

  void sauveEntete()
  {
    m_ofstream<<"#FIG 3.2"<<std::endl
             <<"Portrait"<<std::endl
            <<"Metric"<<std::endl
           <<"A4"<<std::endl
          <<"100.00"<<std::endl
         <<"Single"<<std::endl
        <<"-2"<<std::endl
       <<"1200 2"<<std::endl;
  }


/* void CControlerGMap::sauveFace(const std::vector<Point2d>& pts,
                               int coulFill, int larg, int AProf )
{
  assert(pts.size()>=3); // 3 => polygone à 2 arêtes...


  os<<"2 1 0 " << larg << " "
    << 32+COUL_DART << " " << 32+coulFill << " "
    << (1+AProf) <<" 0 20 0.000 1 0 7 0 0 "
    << nbPts << endl;

  for (int i=0; i<nbPts; ++i)
  {
    os << int(RINT(p[i].getX()+decalageX)) << " "
       << int(RINT(p[i].getY()+decalageY)) << endl;
  }
}
*/

protected:
  std::ofstream m_ofstream;
  uint8_t m_nb_couls;
  bool m_invert_y;
  double m_x_factor;
  int m_x_shift, m_y_shift;
};
////////////////////////////////////////////////////////////////////////////////
#endif // EXPORT_XFIG_H
