// compute_arrangement: a new method to compute arrangement of segments.
// Copyright (C) 2020 CNRS and LIRIS' Establishments (France).
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
// Author(s)     : Guillaume Damiand <guillaume.damiand@liris.cnrs.fr>
//

#ifndef SEGMENT_READERS_H
#define SEGMENT_READERS_H

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <CGAL/Random.h>
#include <CGAL/point_generators_2.h>

///////////////////////////////////////////////////////////////////////////////
/// \brief The Reader class
template<typename Kernel>
class Reader
{
public:
  using FT=typename Kernel::FT;
  using Point=typename Kernel::Point_2;
  using Segment=typename Kernel::Segment_2;

  Reader() : m_cur_segment(0), m_split(false)
  {}

  void split(std::size_t splitLow, std::size_t splitHight)
  {
    m_split=true;
    m_splitLow=splitLow;
    m_splitHight=splitHight;
  }

  virtual ~Reader(){}
  virtual bool reader_ok() const =0;
  virtual Segment get_the_next_segment() =0;

  bool ok() const
  { return reader_ok() && (!m_split || m_cur_segment<m_splitHight); }

  Segment next_segment()
  {
    Segment res;
    do
    {
      res=get_the_next_segment();
      ++m_cur_segment;
    }
    while(reader_ok() &&
          (m_split &&
           (m_cur_segment<m_splitLow || m_cur_segment>m_splitHight)));
    return res;
  }

protected:
  std::size_t m_cur_segment;
  bool m_split;
  std::size_t m_splitLow, m_splitHight;
};
///////////////////////////////////////////////////////////////////////////////
template<typename Kernel>
class Read_from_file: public Reader<Kernel>
{
public:
  using Base=Reader<Kernel>;
  using Segment=typename Base::Segment;
  using Point=typename Base::Point;

  Read_from_file(const std::string& filename): ifs(filename)
  {
    // std::cout<<"Segment file: "<<filename<<std::endl;
    if (!ifs.is_open())
    {
      std::cerr<<"Error opening file '"<<filename<<"'"<<std::endl;
    }
    else if (ifs.peek()==EOF)
    {
      std::cerr<<"Error, empty file '"<<filename<<"'"<<std::endl;
      ifs.close();
    }
  }

  bool reader_ok() const
  { return ifs.is_open(); }

  Segment get_the_next_segment()
  {
    Segment s;
    std::string line;
    double x1, y1, x2, y2;
    do
    {
      std::getline(ifs, line);
      if (ifs.good())
      { line.erase(line.find_last_not_of(" \n\r\t")+1); }
    }
    while (!ifs.fail() && line[0]=='#'); // jump over comment lines

    if (ifs.good())
    {
      std::istringstream is(line);
      is>>x1>>y1>>x2>>y2;
      if (!is.fail())
      {
        if (ifs.peek()==EOF) { ifs.close(); }
        if (x1>x2 || (x1==x2 && y1>y2))
        { std::swap(x1, x2); std::swap(y1, y2); }
        return Segment(Point(x1, y1), Point(x2, y2));
      }
    }
    std::cerr<<"Error when reading file."<<std::endl;
    ifs.close();
    return Segment(Point(0,0),Point(0,0)); // An error occured.
  }

protected:
  std::ifstream ifs;
};
///////////////////////////////////////////////////////////////////////////////
template<typename Kernel>
class Random_segments: public Reader<Kernel>
{
public:
  using Base=Reader<Kernel>;
  using Segment=typename Base::Segment;
  using Point=typename Base::Point;
  using FT=typename Base::FT;

  Random_segments(std::size_t N, CGAL::Random& random,
                  bool size, double L) :
    m_N(N), m_random(random), m_size(size), m_L(L), m_nb_generated(0)
  {
    CGAL_assertion(L>0); // Otherwise, crash of Random_points_in_disc_2 constructor
    std::cout<<"Random seed: "<<random.get_seed();
    if (size) std::cout<<" with size<="<<L;
    std::cout<<std::endl;
  }

  bool reader_ok() const
  { return m_nb_generated<m_N; }

  Segment get_the_next_segment()
  {
    Segment s;
    Point p2;
    typedef CGAL::Creator_uniform_2<FT, Point> Creator;
    CGAL::Random_points_in_disc_2<Point,Creator> rand_in_disk(m_L, m_random);
    if (m_nb_generated==0)
    {
      m_p1=Point(m_random.get_double(0.0, 100.0),
                m_random.get_double(0.0, 100.0));
    }
    if (!m_size)
    { p2=Point(m_random.get_double(0.0, 100.0),
                m_random.get_double(0.0, 100.0)); }
    else
    {
      do
      {
        p2=(*rand_in_disk); ++rand_in_disk;
        p2=Point(m_p1.x()+p2.x(), m_p1.y()+p2.y());
      }
      while(p2.x()<0.0 || p2.x()>100.0 || p2.y()<0.0 || p2.y()>100.0);
    }
    /*    if (random.get_int(0,101)>60 && number_of_segments()>0) // dans 40% des cas
    { // Get an existing segment
      std::size_t existing_s=
          static_cast<std::size_t>(random.get_int
                                   (0,static_cast<int>(number_of_segments())));
      double d1=random.get_double(0., 1.);
      double d2=random.get_double(0., 1.);
      typename MyKernel::Vector_2 vect
          (get_segment(existing_s).source(), get_segment(existing_s).target());
      p1=Point(get_segment(existing_s).source()+vect*d1);
      p2=Point(get_segment(existing_s).target()-vect*d2);
    }
    else if (random.get_int(0,101)>50 && number_of_segments()>0) // dans 50% des cas restants
    {
      std::size_t existing_s=
          static_cast<std::size_t>(random.get_int
                                   (0,static_cast<int>(number_of_segments())));
      p1=get_segment(existing_s).target();
      do
      {
        p2=(*rand_in_disk); ++rand_in_disk;
        p2=Point(p1.x()+p2.x(), p1.y()+p2.y());
      }
      while(p2.x()<0.0 || p2.x()>100.0 || p2.y()<0.0 || p2.y()>100.0);
      }*/
    Segment res;
    if (m_p1.x()>p2.x() || (m_p1.x()==p2.x() && m_p1.y()>p2.y()))
    { res=Segment(p2, m_p1); }
    else  { res=Segment(m_p1, p2); }

    ++m_nb_generated;
    m_p1=p2;
    return res;
    // std::cout<<"Segment "<<p1<<" -> "<<p2<<std::endl;
  }

protected:
  std::size_t m_N;
  CGAL::Random m_random;
  bool m_size;
  double m_L;
  std::size_t m_nb_generated;
  Point m_p1;
};
///////////////////////////////////////////////////////////////////////////////
template<typename Kernel>
class Vector_of_segments: public Reader<Kernel>
{
public:
  using Base=Reader<Kernel>;
  using Segment=typename Base::Segment;
  using Point=typename Base::Point;

  Vector_of_segments(const std::vector<Segment>& all_segments) :
    m_all_segments(all_segments),
    m_nb_generated(0)
  {}

  bool reader_ok() const
  { return m_nb_generated<m_all_segments.size(); }

  Segment get_the_next_segment()
  { return m_all_segments[m_nb_generated++]; }

protected:
  const std::vector<Segment>& m_all_segments;
  std::size_t m_nb_generated;
};
///////////////////////////////////////////////////////////////////////////////
template<typename Kernel>
inline
void fill_segment_array(Reader<Kernel>& reader,
                        std::vector<typename Kernel::Segment_2>& all_segments)
{
  all_segments.clear();
  while (reader.ok())
  {
    typename Kernel::Segment_2 s=reader.next_segment();
    if (s.source()!=s.target())
    { all_segments.push_back(s); }
  }
}
///////////////////////////////////////////////////////////////////////////////
inline std::size_t number_of_lines(const std::string& filename)
{
  std::ifstream in(filename);
  std::size_t nb_lignes=0;
  while(in.ignore(std::numeric_limits<std::streamsize>::max(), '\n'))
  { ++nb_lignes; }
  in.close();
  return nb_lignes;
}
///////////////////////////////////////////////////////////////////////////////
#endif // SEGMENT_READERS_H
