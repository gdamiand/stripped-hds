// compute_arrangement: a new method to compute arrangement of segments.
// Copyright (C) 2020 CNRS and LIRIS' Establishments (France).
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
// Author(s)     : Guillaume Damiand <guillaume.damiand@liris.cnrs.fr>
//

#ifndef SHDS_H
#define SHDS_H

#include <iostream>
#include <thread>
#include <vector>
#include <mutex>
#include <boost/filesystem.hpp>

#include <CGAL/Compact_container.h>
#include <CGAL/Exact_predicates_exact_constructions_kernel.h>

#include "My_construction_subcurve.h"
#include "My_arr_segment_traits_2.h"
#include "My_surface_sweep.h"
#include "My_visitor.h"
#include "memory.h"
#include "CGAL_typedefs.h"
#include "Partial_hds.h"
#include "Hds.h"

////////////////////////////////////////////////////////////////////////////////
class SHds
{
public:
  using Self=SHds;

  typedef uint32_t size_type;
  static const size_type NB_MARKS=Halfedge::NB_MARKS;
  static const size_type INVALID_MARK=NB_MARKS;
  class Exception_no_more_available_mark {};

  SHds():
    m_xmin(0), m_ymin(0), m_xmax(0), m_ymax(0),
    m_nb_strips(0),
    m_number_segments(0),
    m_nb_finite_faces(0)
  { init_all_marks(); }

  void init_all_marks()
  {
    mnb_used_marks = 0;
    mmask_marks.reset();
    for (size_type i=0; i<NB_MARKS; ++i)
    {
      mfree_marks_stack[i]        = i;
      mindex_marks[i]             = i;
      mnb_marked_halfedges[i]     = 0;
      mnb_times_reserved_marks[i] = 0;
    }
  }

  SHds(const std::string& filename,
       std::size_t nb_strips,
       std::size_t nb_threads,
       bool optimized_strips,
       std::vector<double>& times,
       bool crop=false,
       std::vector<double>* xpos_cuts=nullptr):
    m_xmin(0), m_ymin(0), m_xmax(0), m_ymax(0),
    m_nb_strips(nb_strips),
    m_number_segments(0),
    m_nb_finite_faces(0)
  {
    init_all_marks();

    if (times.empty())
    { times.resize(4, 0); }

    // times[0] : global time
    // times[1] : load and dispatch
    // times[2] : compute all partial arrangements
    // times[3] : compute faces

    My_timer global_timer; global_timer.start();
    My_timer local_timer;
    local_timer.start();

    std::vector<ISegment> all_segments;
    std::vector<double>   xpos_strips;

    if (xpos_cuts!=nullptr)
    { compute_strips_v0(filename, all_segments, xpos_strips, xpos_cuts);}
    else if (!optimized_strips)
    { compute_strips_v1(filename, all_segments, xpos_strips); }
    else
    { compute_strips_v2(filename, all_segments, xpos_strips); }

    std::cout<<"strips xpos=(";
    for (std::size_t i=0; i<xpos_strips.size(); ++i)
    { std::cout<<xpos_strips[i]<<"  "; }
    std::cout<<")   #size="<<xpos_strips.size()<<std::endl;

    // Test if definition of strips is valid.
    bool strip_error=(xpos_strips.size()!=number_of_strips()+1);
    for (std::size_t i=1; i<nb_strips; ++i)
    {
      if (xpos_strips[i]<=m_xmin || xpos_strips[i]>=m_xmax ||
          xpos_strips[i]<=xpos_strips[i-1])
      { strip_error=true; }
    }
    if (xpos_strips.front()!=m_xmin || xpos_strips.back()!=m_xmax)
    { strip_error=true; }
    if (strip_error)
    {
      std::cerr<<"[ERROR] in definition of strips: #nbstrips="<<number_of_strips()
              <<std::endl;
      exit(EXIT_FAILURE);
    }

    local_timer.stop();
    times[1]+=local_timer.time();

    // Now compute partial hds, possibly in parallel
    m_tab_partial_hds.resize(number_of_strips());
    std::vector<std::thread> tab_threads;
    tab_threads.reserve(nb_threads);

    std::mutex queue_mutex;
    std::queue<std::size_t> strips_to_process;
    for (std::size_t i=0; i<nb_strips; ++i)
    { strips_to_process.push(i); }

    local_timer.reset();
    local_timer.start();
    for (std::size_t i=0; i<nb_threads; ++i)
    {
      tab_threads.push_back
          (std::thread
           ([i, nb_threads, &queue_mutex, &strips_to_process, &all_segments,
            &xpos_strips, crop,this]()
      {
        std::size_t nb_intersections=0;
        std::ostringstream ss;
        My_timer timer; timer.start();
        std::size_t strip_id;
        queue_mutex.lock();
        while(!strips_to_process.empty())
        {
          strip_id=strips_to_process.front();
          strips_to_process.pop();
          queue_mutex.unlock();
          ss<<strip_id<<" ";

          m_tab_partial_hds[strip_id]=new Partial_hds
              (xpos_strips[strip_id], m_ymin, xpos_strips[strip_id+1], m_ymax,
              strip_id, m_nb_strips);

          typedef My_visitor<My_arr_segment_traits_2<EPECK>> Visitor;
          My_arr_segment_traits_2<EPECK> m_traits;
          Visitor visitor(*m_tab_partial_hds[strip_id]);
          My_surface_sweep_2<Visitor> surface_sweep(&m_traits, &visitor);
          visitor.sweep(all_segments, crop);
          nb_intersections+=visitor.number_of_intersections();
          assert(m_tab_partial_hds[strip_id]->is_valid_external_halfedges());
          queue_mutex.lock();
        }
        queue_mutex.unlock();
        timer.stop();
        print_txt(" [Thread ", i, "]: strips=", ss.str(), "; #intersections=",
                  nb_intersections, "  time=",timer.time());
      }
      ));
    }
    for (std::size_t i=0; i<tab_threads.size(); ++i)
    { tab_threads[i].join(); }
    std::cout<<std::endl;

    local_timer.stop();
    times[2]+=local_timer.time();

    local_timer.reset();
    local_timer.start();
    build_faces_array();
    local_timer.stop();
    times[3]+=local_timer.time();

    global_timer.stop();
    times[0]+=global_timer.time();
  }

  ~SHds()
  {
    for (auto it=m_tab_partial_hds.begin(),
         itend=m_tab_partial_hds.end(); it!=itend; ++it)
    { delete *it; *it=nullptr; }
  }

  std::size_t number_of_strips() const
  { return m_nb_strips; }
  std::size_t number_of_faces() const
  { return m_faces.size(); }
  std::size_t number_of_finite_faces() const
  { return m_nb_finite_faces; }

  double xmin() const
  { return m_xmin; }
  double ymin() const
  { return m_ymin; }
  double xmax() const
  { return m_xmax; }
  double ymax() const
  { return m_ymax; }

  HDS& hds(std::size_t nb)
  {
    assert(nb<number_of_strips());
    return m_tab_partial_hds[nb]->hds();
  }
  const HDS& hds(std::size_t nb) const
  {
    assert(nb<number_of_strips());
    return m_tab_partial_hds[nb]->hds();
  }

  HDS& hds(const Global_halfedge& phe)
  { return hds(phe.strip_id()); }
  const HDS& hds(const Global_halfedge& phe) const
  { return hds(phe.strip_id()); }

  Partial_hds& partial_hds(std::size_t nb)
  {
    assert(nb<number_of_strips());
    return *(m_tab_partial_hds[nb]);
  }
  const Partial_hds& partial_hds(std::size_t nb) const
  {
    assert(nb<number_of_strips());
    return *(m_tab_partial_hds[nb]);
  }

  Partial_hds& partial_hds(const Global_halfedge& phe)
  { return partial_hds(phe.strip_id()); }
  const Partial_hds& partial_hds(const Global_halfedge& phe) const
  { return partial_hds(phe.strip_id()); }

  void clear()
  {
    m_xmin=m_ymin=m_xmax=m_ymax=0;
    m_nb_strips=m_number_segments=0;
    for (auto it=m_tab_partial_hds.begin(),
         itend=m_tab_partial_hds.end(); it!=itend; ++it)
    { delete *it; *it=nullptr; }
    m_faces.clear();
    init_all_marks();
  }

  void read_segments_and_compute_bbox(const std::string& filename,
                                      std::vector<ISegment>& all_segments)
  {
    all_segments.clear();
    all_segments.reserve(number_of_lines(filename));

    CGAL::Bbox_2 bbox;
    m_number_segments=0;
    Read_from_file<EPICK> reader(filename);
    while (reader.ok())
    {
      EPICK::Segment_2 s=reader.next_segment();
      all_segments.push_back(s);
      if (m_number_segments==0) { bbox=s.bbox(); }
      else { bbox+=s.bbox(); }
      ++m_number_segments;
    }
    if (m_number_segments==0)
    {
      std::cout<<"Empty set of segments for file "<<filename<<", abort."<<std::endl;
      exit(EXIT_FAILURE);
    }

    double deltax=(bbox.xmax()-bbox.xmin())/10000.; // 0.01 %
    double deltay=(bbox.ymax()-bbox.ymin())/10000.; // 0.01 %
    m_xmin=bbox.xmin()-deltax;
    m_ymin=bbox.ymin()-deltay;
    m_xmax=bbox.xmax()+deltax;
    m_ymax=bbox.ymax()+deltay;
  }

  void compute_strips_v0(const std::string& filename,
                        std::vector<ISegment>& all_segments,
                        std::vector<double>& xpos_strips,
                        std::vector<double>* xpos_cuts)
  {
    if (xpos_cuts==nullptr || xpos_cuts->size()!=number_of_strips()-1)
    {
      std::cout<<"Error in compute_strips_v0, switch to default strip computation."<<std::endl;
      compute_strips_v1(filename, all_segments, xpos_strips);
      return;
    }

    read_segments_and_compute_bbox(filename, all_segments);

    xpos_strips.clear();
    xpos_strips.resize(number_of_strips()+1, 0);

    xpos_strips[0]=m_xmin;
    for (std::size_t i=0; i<xpos_cuts->size(); ++i)
    { xpos_strips[i+1]=xpos_cuts->at(i); }
    xpos_strips[number_of_strips()]=m_xmax;
  }

  void compute_strips_v1(const std::string& filename,
                        std::vector<ISegment>& all_segments,
                        std::vector<double>& xpos_strips)
  {
    read_segments_and_compute_bbox(filename, all_segments);

    xpos_strips.clear();
    xpos_strips.resize(number_of_strips()+1, 0);

    xpos_strips[0]=m_xmin;
    for (std::size_t i=1; i<number_of_strips(); ++i)
    { xpos_strips[i]=m_xmin+((m_xmax-m_xmin)*i)/number_of_strips(); }
    xpos_strips[number_of_strips()]=m_xmax;
  }

  void compute_strips_v2(const std::string& filename,
                        std::vector<ISegment>& all_segments,
                        std::vector<double>& xpos_strips)
  {
    constexpr std::size_t array_size=1000;

    read_segments_and_compute_bbox(filename, all_segments);

    // Now compute histogram of segments (only x coordinates)
    std::vector<std::size_t> nbsegments(array_size,0);
    std::size_t sum=0;
    for (std::size_t i=0; i<all_segments.size(); ++i)
    {
      ISegment& s=all_segments[i];
      std::size_t imin=floor((array_size*(s.source().x()-m_xmin))/(m_xmax-m_xmin));
      std::size_t imax=floor((array_size*(s.target().x()-m_xmin))/(m_xmax-m_xmin));
      for (; imin<=imax; ++imin)
      {
        assert(imin<array_size);
        ++nbsegments[imin]; ++sum;
      }
    }

    // Now we need to dispatch sum parts of segment into number_of_threads strips.
    std::size_t cursum=0;
    xpos_strips.clear();
    xpos_strips.reserve(number_of_strips()+1);
    xpos_strips.push_back(m_xmin);
    for (std::size_t i=0; i<array_size && xpos_strips.size()<number_of_strips(); ++i)
    {
      if (cursum+nbsegments[i]>sum/number_of_strips())
      {
        xpos_strips.push_back(m_xmin+i*(m_xmax-m_xmin)/array_size);
        cursum=0;
      }
      cursum+=nbsegments[i];
    }
    xpos_strips.push_back(m_xmax);
    assert(xpos_strips.size()==number_of_strips()+1);
  }

  // phe is a left_to_right external halfedge.
  // Move it in the halfedge with the same id in the left strip.
  void move_to_same_halfedge_in_left_strip(Global_halfedge& phe) const
  {
    assert(partial_hds(phe).is_external(phe.halfedge()));
    assert(hds(phe).left_to_right(phe.halfedge()));
    phe.go_to_left_strip();
    auto find_external=partial_hds(phe).m_right_external_halfedges.find(hds(phe).edge_id(phe.halfedge()));
    assert(find_external!=partial_hds(phe).m_right_external_halfedges.end());
    phe.halfedge()=hds(phe).opposite(find_external->second);
  }

  // phe is a right_to_left external halfedge.
  // Move it in the halfedge with the same id in the right strip.
  void move_to_same_halfedge_in_right_strip(Global_halfedge& phe) const
  {
    assert(partial_hds(phe).is_external(phe.halfedge()));
    assert(hds(phe).right_to_left(phe.halfedge()));
    phe.go_to_right_strip();
    auto find_external=partial_hds(phe).m_left_external_halfedges.find(-hds(phe).edge_id(phe.halfedge()));
    assert(find_external!=partial_hds(phe).m_left_external_halfedges.end());
    phe.halfedge()=hds(phe).opposite(find_external->second);
  }

  void move_to_same_halfedge_in_adjacent_strip(Global_halfedge& phe) const
  {
    assert(partial_hds(phe).is_external(phe.halfedge()));
    if (hds(phe).left_to_right(phe.halfedge()))
    { move_to_same_halfedge_in_left_strip(phe); }
    else
    { move_to_same_halfedge_in_right_strip(phe); }
  }

  // phe is a parallel halfedge. If this is a external halfedge, move to the
  // corresponding non external halfedge.
  void move_to_non_external(Global_halfedge& phe) const
  {
    if (partial_hds(phe).is_external(phe.halfedge()))
    {
      if (hds(phe).left_to_right(phe.halfedge()))
      {
        do
        { move_to_same_halfedge_in_left_strip(phe); }
        while(partial_hds(phe).is_external(phe.halfedge()));
      }
      else
      {
        do
        { move_to_same_halfedge_in_right_strip(phe); }
        while(partial_hds(phe).is_external(phe.halfedge()));
      }
    }
  }

  // The following move_to_xxx methods traverse the shds jumping over external
  // halfedges. This allows to see the stripped hds like a normal hds.
  void move_to_previous(Global_halfedge& phe) const
  {
    assert(!partial_hds(phe).is_external(phe.halfedge()));
    hds(phe.strip_id()).move_to_prev_around_vertex(phe.halfedge());
    move_to_opposite(phe);
  }
  void move_to_next(Global_halfedge& phe) const
  {
    assert(!partial_hds(phe).is_external(phe.halfedge()));
    move_to_opposite(phe);
    hds(phe.strip_id()).move_to_next_around_vertex(phe.halfedge());
  }
  void move_to_opposite(Global_halfedge& phe) const
  {
    assert(!partial_hds(phe).is_external(phe.halfedge()));
    hds(phe.strip_id()).move_to_opposite(phe.halfedge());
    move_to_non_external(phe);
  }

  Global_halfedge next(const Global_halfedge& phe) const
  {
    Global_halfedge res(phe);
    move_to_next(res);
    return res;
  }

  Global_halfedge previous(const Global_halfedge& phe) const
  {
    Global_halfedge res(phe);
    move_to_previous(res);
    return res;
  }

  Global_halfedge opposite(const Global_halfedge& phe) const
  {
    Global_halfedge res(phe);
    move_to_opposite(res);
    return res;
  }

  // The following local_move_to_xxx methods traverse the hds by considering
  // also external halfedges (contrary to methods above).
  void local_move_to_previous(Global_halfedge& phe) const
  {
    if (partial_hds(phe).is_external(phe.halfedge()))
    { move_to_same_halfedge_in_adjacent_strip(phe); }
    else
    {
      hds(phe.strip_id()).move_to_prev_around_vertex(phe.halfedge());
      hds(phe.strip_id()).move_to_opposite(phe.halfedge());
    }
  }
  void local_move_to_next(Global_halfedge& phe) const
  {
    hds(phe.strip_id()).move_to_opposite(phe.halfedge());
    if(partial_hds(phe).is_external(phe.halfedge()))
    {
      move_to_same_halfedge_in_adjacent_strip(phe);
      hds(phe.strip_id()).move_to_opposite(phe.halfedge());
    }
    else
    {
      hds(phe.strip_id()).move_to_next_around_vertex(phe.halfedge());
    }
  }
  void local_move_to_opposite(Global_halfedge& phe) const
  {
    phe.m_hh=hds(phe.strip_id()).opposite(phe.halfedge());
  }
  Global_halfedge local_next(const Global_halfedge& phe) const
  {
    Global_halfedge res(phe);
    local_move_to_next(res);
    return res;
  }

  Global_halfedge local_previous(const Global_halfedge& phe) const
  {
    Global_halfedge res(phe);
    local_move_to_previous(res);
    return res;
  }

  Global_halfedge local_opposite(const Global_halfedge& phe) const
  {
    Global_halfedge res(phe);
    local_move_to_opposite(res);
    return res;
  }

  const EPoint& point(const Global_halfedge& phe) const
  { return hds(phe.strip_id()).point(phe.halfedge()); }

  Vertex_handle vertex(Halfedge_handle hh)
  { return hh->m_vertex; }
  Vertex_handle vertex(const Global_halfedge& phh)
  { return vertex(phh.halfedge()); }

  Face_handle create_face()
  { return m_faces.emplace(); }
  Face_handle face(Halfedge_handle hh)
  { return hh->m_face; }
  Face_handle face(const Global_halfedge& phh)
  { return face(phh.halfedge()); }
  void set_face(Halfedge_handle hh, Face_handle fh)
  { hh->m_face=fh; }
  const CGAL::Compact_container<Face>& faces() const
  { return m_faces; }

  std::size_t number_of_halfedges() const
  {
    std::size_t res=0;
    for (std::size_t i=0; i<number_of_strips(); ++i)
    { res+=hds(i).number_of_halfedges(); }
    return res;
  }

  std::size_t number_of_non_external_halfedges() const
  {
    std::size_t res=0;
    for (std::size_t i=0; i<number_of_strips(); ++i)
    { res+=partial_hds(i).number_of_non_externals(); }
    return res;
  }

  std::size_t number_of_external_halfedges() const
  {
    std::size_t res=0;
    for (std::size_t i=0; i<number_of_strips(); ++i)
    { res+=m_tab_partial_hds[i]->number_of_externals(); }
    return res;
  }

  std::size_t number_of_vertices() const
  {
    std::size_t res=0;
    for (std::size_t i=0; i<number_of_strips(); ++i)
    { res+=m_tab_partial_hds[i]->number_of_vertices(); }
    return res;
  }

  std::size_t number_of_edges() const
  { return number_of_non_external_halfedges()/2; }

  /** Count the number of used marks.
   * @return the number of used marks.
   */
  size_type number_of_used_marks() const
  { return mnb_used_marks; }

  /** Test if a given mark is reserved.
   *  @return true iff the mark is reserved (ie in used).
   */
  bool is_reserved(size_type amark) const
  {
    CGAL_assertion(amark<NB_MARKS);
    return (mnb_times_reserved_marks[amark]!=0);
  }

  /**  Count the number of marked halfedges for a given mark.
   * @param amark the mark index.
   * @return the number of marked halfedges for amark.
   */
  size_type number_of_marked_halfedges(size_type amark) const
  {
    CGAL_assertion( is_reserved(amark) );
    return mnb_marked_halfedges[amark];
  }

  /**  Count the number of unmarked halfedges for a given mark.
   * @param amark the mark index.
   * @return the number of unmarked halfedges for amark.
   */
  size_type number_of_unmarked_halfedges(size_type amark) const
  {
    CGAL_assertion( is_reserved(amark) );
    return number_of_halfedges()-number_of_marked_halfedges(amark);
  }

  /** Test if all the halfedges are unmarked for a given mark.
   * @param amark the mark index.
   * @return true iff all the halfedges are unmarked for amark.
   */
  bool is_whole_hds_unmarked(size_type amark) const
  { return number_of_marked_halfedges(amark)==0; }

  /** Test if all the halfedges are marked for a given mark.
   * @param amark the mark index.
   * @return true iff all the halfedges are marked for amark.
   */
  bool is_whole_hds_marked(size_type amark) const
  {  return number_of_marked_halfedges(amark)==number_of_halfedges(); }

  /** Reserve a new mark.
   * Get a new free mark and return its index.
   * All the halfedges are unmarked for this mark.
   * @return the index of the new mark.
   * @pre mnb_used_marks < NB_MARKS
   */
  size_type get_new_mark() const
  {
    if (mnb_used_marks == NB_MARKS)
    {
      std::cerr << "Not enough Boolean marks: "
        "increase NB_MARKS." << std::endl;
      std::cerr << "  (exception launched)" << std::endl;
      throw Exception_no_more_available_mark();
    }

    size_type m = mfree_marks_stack[mnb_used_marks];
    mused_marks_stack[mnb_used_marks] = m;

    mindex_marks[m] = mnb_used_marks;
    mnb_times_reserved_marks[m]=1;

    ++mnb_used_marks;
    CGAL_assertion(is_whole_hds_unmarked(m));

    return m;
  }

  /** Increase the number of times a mark is reserved.
   *  @param amark the mark to share.
   */
  void share_a_mark(size_type amark) const
  {
    CGAL_assertion( is_reserved(amark) );
    ++mnb_times_reserved_marks[amark];
  }

  /** @return the number of times a mark is reserved.
   *  @param amark the mark to share.
   */
  size_type get_number_of_times_mark_reserved(size_type amark) const
  {
    CGAL_assertion( amark<NB_MARKS );
    return mnb_times_reserved_marks[amark];
  }

  /** Negate the mark of all the halfedges for a given mark.
   * After this call, all the marked halfedges become unmarked and all the
   * unmarked halfedges become marked (in constant time operation).
   * @param amark the mark index
   */
  void negate_mark(size_type amark) const
  {
    CGAL_assertion( is_reserved(amark) );

    mnb_marked_halfedges[amark] = number_of_halfedges()-mnb_marked_halfedges[amark];

    mmask_marks.flip(amark);
  }

  /// Return the mark value of he a given mark number.
  bool get_he_mark(Halfedge_const_handle he, size_type amark) const
  { return he->get_mark(amark); }

  /// Set the mark of a given mark number to a given value.
  void set_he_mark(Halfedge_const_handle he, size_type amark, bool avalue) const
  { he->set_mark(amark, avalue); }

  /// Flip the mark of a given mark number to a given value.
  void flip_he_mark(Halfedge_const_handle he, size_type amark) const
  { he->flip_mark(amark); }

  std::bitset<NB_MARKS> get_he_marks(Halfedge_const_handle he) const
  { return he->get_marks(); }
  void set_he_marks(Halfedge_const_handle he,
                    const std::bitset<NB_MARKS>& amarks) const
  { he->set_marks(amarks); }

  /** Test if a given halfedge is marked for a given mark.
   * @param he the halfedge to test.
   * @param amark the given mark.
   * @return true iff halfedge is marked for the mark amark.
   */
  bool is_marked(Halfedge_const_handle he, size_type amark) const
  {
    CGAL_assertion( is_reserved(amark) );

    return get_he_mark(he, amark)!=mmask_marks[amark];
  }

  /** Set the mark of a given halfedge to a state (on or off).
   * @param ahalfedge the halfedge.
   * @param amark the given mark.
   * @param astate the state of the mark (on or off).
   */
  void set_mark_to(Halfedge_const_handle he, size_type amark,
                   bool astate) const
  {
    CGAL_assertion(is_reserved(amark));

    if (is_marked(he, amark)!=astate)
    {
      if (astate) ++mnb_marked_halfedges[amark];
      else --mnb_marked_halfedges[amark];

      flip_he_mark(he, amark);
    }
  }

  /** Mark the given halfedge.
   * @param he the halfedge.
   * @param amark the given mark.
   */
  void mark(Halfedge_const_handle he, size_type amark) const
  {
    CGAL_assertion(is_reserved(amark));

    if (is_marked(he, amark)) return;

    ++mnb_marked_halfedges[amark];
    flip_he_mark(he, amark);
  }

  /** Unmark the given halfedge.
   * @param he the halfedge.
   * @param amark the given mark.
   */
  void unmark(Halfedge_const_handle he, size_type amark) const
  {
    CGAL_assertion(is_reserved(amark));

    if (!is_marked(he, amark)) return;

    --mnb_marked_halfedges[amark];
    flip_he_mark(he, amark);
  }

  bool is_marked(const Global_halfedge& phe, size_type amark) const
  { return is_marked(phe.halfedge(), amark); }
  void mark(const Global_halfedge& phe, size_type amark) const
  { mark(phe.halfedge(), amark); }
  void unmark(const Global_halfedge& phe, size_type amark) const
  { unmark(phe.halfedge(), amark); }

  /** Unmark all the halfedges of the hds for a given mark.
   * If all the halfedges are marked or unmarked, this operation takes O(1)
   * operations, otherwise it traverses all the halfedges of the map.
   * @param amark the given mark.
   */
  void unmark_all(size_type amark) const
  {
    CGAL_assertion( is_reserved(amark) );

    if ( is_whole_hds_marked(amark) )
    { negate_mark(amark); }
    else if ( !is_whole_hds_unmarked(amark) )
    {
      for (std::size_t i=0; i<number_of_strips(); ++i)
      {
        for (auto it(hds(i).halfedges().begin()), itend(hds(i).halfedges().end());
             it!=itend; ++it)
          unmark(it, amark);
      }
    }
    CGAL_assertion(is_whole_hds_unmarked(amark));
  }

  /** Free a given mark, previously calling unmark_all.
   * @param amark the given mark.
   */
  void free_mark(size_type amark) const
  {
    CGAL_assertion( is_reserved(amark) );

    if ( mnb_times_reserved_marks[amark]>1 )
    {
      --mnb_times_reserved_marks[amark];
      return;
    }

    unmark_all(amark);

    // 1) We remove amark from the array mused_marks_stack by
    //    replacing it with the last mark in this array.
    mused_marks_stack[mindex_marks[amark]] =
      mused_marks_stack[--mnb_used_marks];
    mindex_marks[mused_marks_stack[mnb_used_marks]] =
      mindex_marks[amark];

    // 2) We add amark in the array mfree_marks_stack and update its index.
    mfree_marks_stack[ mnb_used_marks ] = amark;
    mindex_marks[amark] = mnb_used_marks;

    mnb_times_reserved_marks[amark]=0;
  }

  // Build the faces of the given shds (and the father/son relations between holes)
  // Return the number of finite faces.
  std::size_t build_faces_array()
  {
    // display_info();

    Face_handle m_root=create_face();
    m_root->m_finite=false; // Infinite face

    std::size_t finite_faces=0;
    auto treated=get_new_mark();
    Face_handle newf, father;
    Global_halfedge dh, initdh, odh;
    Halfedge_handle firsthalfedge, fatherhalfedge;
    std::stack<Global_halfedge> totreat;

    // Iterate through all inner points, ordered from left to right
    for (std::size_t i=0; i<number_of_strips(); ++i)
    {
      for (auto it=hds(i).vertices().begin(),
           itend=hds(i).vertices().end(); it!=itend; ++it)
      {
        if (!partial_hds(i).is_external(it->first_halfedge()) &&
            !is_marked(it->first_halfedge(), treated))
        { // first_halfedge belongs necessarily to one infinite face.
          firsthalfedge=it->first_halfedge();
          newf=create_face();

          // std::size_t nbhalfedges=0;
          initdh.set(i, firsthalfedge);
          dh=initdh;
          do
          {
            // ++nbhalfedges;
            mark(dh, treated);
            set_face(dh.halfedge(), newf);
            odh=local_opposite(dh);
            if (!is_marked(odh, treated))
            { totreat.push(odh); }
            local_move_to_next(dh);
          }
          while(dh!=initdh);

          fatherhalfedge=it->above_halfedge();
          father=nullptr;
          if (fatherhalfedge==nullptr)
          { father=m_root; }
          else
          {
            assert(face(fatherhalfedge)!=nullptr);

            if (face(fatherhalfedge)->m_finite)
            { // Father is the halfedge of the finite face containing this hole
              father=face(fatherhalfedge);
            }
            else
            { // Father is a halfedge of another hole, inside the same finite face
              // This hole was already considered before.
              assert(face(fatherhalfedge)->m_father!=nullptr);
              father=face(fatherhalfedge)->m_father;
            }
          }
          assert(father==m_root || father->m_finite);
          newf->set(i, firsthalfedge, father, false); // Infinite face
          father->m_son.push_back(newf);

          // Now we iterate through the cc containing it->first_halfedge().
          // Each new face is necessarily a finite one.
          while(!totreat.empty())
          {
            initdh=totreat.top(); totreat.pop();
            if (!is_marked(initdh, treated))
            {
              newf=create_face();
              newf->set(initdh.strip_id(), initdh.halfedge(), nullptr, true); // Finite face
              ++finite_faces;

              // nbhalfedges=0;
              dh=initdh;
              do
              {
                // ++nbhalfedges;
                mark(dh, treated);
                set_face(dh.halfedge(), newf);
                odh=local_opposite(dh);
                if (!is_marked(odh, treated))
                { totreat.push(odh); }
                local_move_to_next(dh);
              }
              while(dh!=initdh);
            }
          }
        }
      }
    }
    assert(is_whole_hds_marked(treated));
    free_mark(treated);

    m_nb_finite_faces=finite_faces;
    return finite_faces;
  }

  std::size_t traverse_all_faces() const
  {
    std::size_t nb=0;
    for (auto itf=faces().begin(), itfend=faces().end(); itf!=itfend; ++itf)
    {
      if (itf->finite() || itf==faces().begin())
      {
        if (itf!=faces().begin())
        { // Because the root face does not have any first_halfedge
          Global_halfedge dhinit(itf->strip_id(), itf->first_halfedge());
          Global_halfedge dh=dhinit;
          do
          {
            ++nb;
            move_to_next(dh);
          }
          while(dh!=dhinit);
        }
        for (auto its=itf->sons().begin(), itsend=itf->sons().end();
             its!=itsend; ++its)
        {
          Global_halfedge dhinit((*its)->strip_id(), (*its)->first_halfedge());
          Global_halfedge dh=dhinit;
          do
          {
            ++nb;
            move_to_next(dh);
          }
          while(dh!=dhinit);
        }
      }
    }
    return nb;
  }

  void display_info() const
  {
    std::size_t nbexternal=0, nbleft=0, nbright=0, nbhalfedges=0, nbvertices=0, sumsegments=0;

    std::cout<<"#halfedges, #vertices #input-segments (#left-external, #right-externals) for each strip: "<<std::endl;
    for (std::size_t i=0; i<number_of_strips(); ++i)
    {
      std::cout<<m_tab_partial_hds[i]->hds().number_of_halfedges()
              <<" "<<m_tab_partial_hds[i]->hds().number_of_vertices()
             <<" "<<m_tab_partial_hds[i]->number_of_input_segments()
             <<" ("
            <<m_tab_partial_hds[i]->number_of_left_externals()
           <<", "
           <<m_tab_partial_hds[i]->number_of_right_externals()
           <<")  | ";
      nbhalfedges+=m_tab_partial_hds[i]->hds().number_of_halfedges();
      nbvertices+=m_tab_partial_hds[i]->hds().number_of_vertices();
      nbleft+=m_tab_partial_hds[i]->m_left_external_halfedges.size();
      nbright+=m_tab_partial_hds[i]->m_right_external_halfedges.size();
      nbexternal+=m_tab_partial_hds[i]->number_of_externals();
      sumsegments+=m_tab_partial_hds[i]->number_of_input_segments();
    }

    std::cout<<std::endl<<"Total: #halfedges="<<nbhalfedges
            <<", #vertices="<<nbvertices<<", #faces="<<m_faces.size()
           <<" (#finite-faces="<<m_nb_finite_faces
          <<"), #nbexternal="<<nbexternal
         <<" (left="<<nbleft<<", right="<<nbright<<")"<<std::endl;
    std::cout<<"Nb input segments: "<<m_number_segments
            <<", Sum segments in strips: "<<sumsegments<<std::endl;
     std::cout<<"CURRENT RSS: "<<getCurrentRSS()<<" bytes (i.e. "<<
               double(getCurrentRSS())/double(1024*1024)<<" mega-bytes and "<<
                double(getCurrentRSS())/double(1024*1024*1024)<<" giga-bytes)"<<std::endl;
  }

  friend std::ostream& operator<<(std::ostream& os,
                                  const SHds& pa)
  {
    os<<pa.m_xmin<<" "<<pa.m_ymin<<" "<<pa.m_xmax<<" "<<pa.m_ymax<<" "
     <<pa.m_nb_strips<<" "<<pa.m_number_segments<<std::endl;

    for (auto it=pa.m_tab_partial_hds.begin(),
         itend=pa.m_tab_partial_hds.end(); it!=itend; ++it)
    { os<<**it<<std::endl; }

    return os;
  }

  friend std::istream& operator>>(std::istream& is,
                                  SHds& pa)
  {
    pa.clear();

    is>>pa.m_xmin>>pa.m_ymin>>pa.m_xmax>>pa.m_ymax
     >>pa.m_nb_strips>>pa.m_number_segments;

    pa.m_tab_partial_hds.resize(pa.m_nb_strips);
    for (std::size_t i=0; i<pa.m_nb_strips; ++i)
    {
      pa.m_tab_partial_hds[i]=new Partial_hds;
      is>>*(pa.m_tab_partial_hds[i]);
    }

    return is;
  }

  // Load the streamed files; filename contains a % which will be replaced
  // by the number of the strip.
  void load_streamed_files(const std::string& filename)
  {
    m_xmin=m_ymin=m_xmax=m_ymax=0.;
    m_number_segments=0;
    m_tab_partial_hds.clear();

    std::size_t i=0;
    std::string real_file=filename;
    real_file.replace(real_file.find("%"), 1, std::to_string(i));
    while(boost::filesystem::exists(boost::filesystem::path(real_file)))
    {
      m_tab_partial_hds.push_back(new Partial_hds);
      std::ifstream is(real_file);
      is>>*(m_tab_partial_hds.back());
      is.close();

      if (m_tab_partial_hds.size()==1)
      {
        m_xmin=CGAL::to_double(m_tab_partial_hds.back()->xmin());
        m_ymin=CGAL::to_double(m_tab_partial_hds.back()->ymin());
        m_xmax=CGAL::to_double(m_tab_partial_hds.back()->xmax());
        m_ymax=CGAL::to_double(m_tab_partial_hds.back()->ymax());
      }
      else
      {
        m_xmin=std::min(m_xmin, CGAL::to_double(m_tab_partial_hds.back()->xmin()));
        m_ymin=std::min(m_ymin, CGAL::to_double(m_tab_partial_hds.back()->ymin()));
        m_xmax=std::max(m_xmax, CGAL::to_double(m_tab_partial_hds.back()->xmax()));
        m_ymax=std::max(m_ymax, CGAL::to_double(m_tab_partial_hds.back()->ymax()));
      }
      m_number_segments+=(m_tab_partial_hds.back()->hds().number_of_halfedges())/2;

      ++i;
      real_file=filename;
      real_file.replace(real_file.find("%"), 1, std::to_string(i));
    }

    m_nb_strips=m_tab_partial_hds.size();

    for (std::size_t i=0; i<m_tab_partial_hds.size(); ++i)
    {
      m_tab_partial_hds[i]->m_strip_id=i;
      m_tab_partial_hds[i]->m_nb_strips=m_nb_strips;
      m_tab_partial_hds[i]->m_min=EPoint(m_tab_partial_hds[i]->m_minx,
                                                m_ymin);
      m_tab_partial_hds[i]->m_max=EPoint(m_tab_partial_hds[i]->m_maxx,
                                                m_ymax);
    }
  }

  // Export the current stripped arrangement into the given filename.
  // If draw_external draw external edges, otherwise they are not drawn.
  // if nbstrips!=0, draw only the first nbstrips. Otherwise draw all strips.
  void export_to_xfig(const std::string& filename,
                      bool invert_y=true, double xfig_x_size=50000,
                      std::size_t nbstrips=0,
                      const CGAL::Color& color_segments=CGAL::Color(40,40,220),
                      const CGAL::Color& color_disks=CGAL::Color(220,40,40),
                      const CGAL::Color& color_externals=CGAL::Color(90,190,110),
                      const CGAL::Color& color_borders=CGAL::Color(150,150,150),
                      uint16_t width_segments=2, uint16_t radius_disks=30,
                      uint16_t width_externals=2, uint16_t width_borders=1,
                      uint16_t space_between_strips=100)
  {
    Export_xfig export_xfig(filename, (m_xmax-m_xmin),
                            xfig_x_size,
                            invert_y);

    uint8_t coul_segments=export_xfig.add_color(color_segments);
    uint8_t coul_disks=export_xfig.add_color(color_disks);
    uint8_t coul_externals=export_xfig.add_color(color_externals);
    uint8_t coul_borders=export_xfig.add_color(color_borders);

    double dx=-m_xmin;
    double dy=-m_ymin;

    std::size_t i=0;
    for (auto it=m_tab_partial_hds.begin(),
         itend=m_tab_partial_hds.end();
         (nbstrips==0 || i<nbstrips) && it!=itend; ++it, ++i)
    {
      export_xfig.set_x_shift(i*space_between_strips);

      if (width_externals>0)
      {
        double x1=CGAL::to_double((*it)->xmin());
        double y1, x2, y2;
        for (auto itc=(*it)->m_left_external_halfedges.begin(),
             itcend=(*it)->m_left_external_halfedges.end(); itc!=itcend; ++itc)
        {
          Global_halfedge h1((*it)->strip_id(), itc->second);
          move_to_non_external(h1);
          Global_halfedge h2=opposite(h1);
          move_to_non_external(h2);

          EPECK::Line_2 line(point(h1), point(h2));
          y1=CGAL::to_double(line.y_at_x((*it)->xmin()));

          if (h2.strip_id()==(*it)->strip_id())
          { // Non traversing segment
            x2=CGAL::to_double(point(h2).x());
            y2=CGAL::to_double(point(h2).y());
          }
          else
          { // Traversing segment
            x2=CGAL::to_double((*it)->xmax());
            y2=CGAL::to_double(line.y_at_x((*it)->xmax()));
          }

          export_xfig.segment(x1+dx, y1+dy, x2+dx, y2+dy,
                              coul_externals, width_externals, false, 100);
        }

        x1=CGAL::to_double((*it)->xmax());
        for (auto itc=(*it)->m_right_external_halfedges.begin(),
             itcend=(*it)->m_right_external_halfedges.end(); itc!=itcend; ++itc)
        {
          Global_halfedge h1((*it)->strip_id(), itc->second);
          move_to_non_external(h1);
          Global_halfedge h2=opposite(h1);
          move_to_non_external(h2);

          EPECK::Line_2 line(point(h1), point(h2));
          y1=CGAL::to_double(line.y_at_x((*it)->xmax()));

          if (h2.strip_id()==(*it)->strip_id())
          { // Non traversing segment; traversing segments were alreary drawn above
            x2=CGAL::to_double(point(h2).x());
            y2=CGAL::to_double(point(h2).y());

            export_xfig.segment(x1+dx, y1+dy, x2+dx, y2+dy,
                                coul_externals, width_externals, false, 100);
          }
        }
      }

      (*it)->export_to_xfig(export_xfig,
                            dx, dy,
                            coul_segments, coul_disks, coul_borders,
                            width_segments, radius_disks, width_borders);
    }
  }

protected:
  double m_xmin, m_ymin, m_xmax, m_ymax;
  std::size_t m_nb_strips;
  std::size_t m_number_segments;
  std::size_t m_nb_finite_faces;

  std::vector<Partial_hds*>     m_tab_partial_hds;
  CGAL::Compact_container<Face> m_faces;

  /// Number of times each mark is reserved. 0 if the mark is free.
  mutable size_type mnb_times_reserved_marks[NB_MARKS];

  /// Mask marks to know the value of unmark halfedge, for each index i.
  mutable std::bitset<NB_MARKS> mmask_marks;

  /// Number of used marks.
  mutable size_type mnb_used_marks;

  /// Index of each mark, in mfree_marks_stack or in mfree_marks_stack.
  mutable size_type mindex_marks[NB_MARKS];

  /// "Stack" of free marks.
  mutable size_type mfree_marks_stack[NB_MARKS];

  /// "Stack" of used marks.
  mutable size_type mused_marks_stack[NB_MARKS];

  /// Number of marked halfedges for each used marks.
  mutable size_type mnb_marked_halfedges[NB_MARKS];
};
////////////////////////////////////////////////////////////////////////////////

#endif // SHDS_H
