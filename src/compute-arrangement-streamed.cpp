// compute_arrangement: a new method to compute arrangement of segments.
// Copyright (C) 2020 CNRS and LIRIS' Establishments (France).
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
// Author(s)     : Guillaume Damiand <guillaume.damiand@liris.cnrs.fr>
//

#include <cassert>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stack>
#include <tuple>
#include <list>

#include <CGAL/Memory_sizer.h>
#include <CGAL/assertions.h>
#include <CGAL/Surface_sweep_2.h>
#include <CGAL/Surface_sweep_2_algorithms.h>
#include <CGAL/Arr_segment_traits_2.h>

#include "CGAL_typedefs.h"
#include "Segment_readers.h"
#include "My_visitor.h"
#include "SHds.h"
#include "SHds_to_lcc.h"
#include "SHds_to_cgal_arrangement.h"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
[[ noreturn ]] void usage(int /*argc*/, char** argv)
{
  // Name
  std::cout<<"Name"<<std::endl;
  std::cout<<"        "<<argv[0]<<" - a new method to compute arrangement of segments, streamed version";
  std::cout<<std::endl<<std::endl;
  // Synopsis
  std::cout<<"SYNOPSIS"<<std::endl;
  std::cout<<"        "<<argv[0]<<" [--help|-h|-?] [-t1 filename] [-nbs N]"
           <<std::endl<<std::endl;
  // Description
  std::cout<<"DESCRIPTION"<<std::endl;
  std::cout<<"        "<<"Compute the arrangement of segments given in the filename."
           <<std::endl<<std::endl;
  // Options
  std::cout<<"        --help, -h, -?"<<std::endl
           <<"                display this help and exit."
           <<std::endl<<std::endl;
  std::cout<<"        -t1 filename"
           <<std::endl
           <<"                load the text segment file, and creates the arrangement: required."
           <<std::endl<<std::endl;
  std::cout<<"        -nbs N"
           <<std::endl
           <<"                fix the number of segments to load for one strip."
           <<std::endl<<std::endl;
 // Author
  std::cout<<"AUTHOR"<<std::endl;
  std::cout<<"        "<<"Written by Guillaume Damiand."
           <<std::endl<<std::endl;
  // Copyright
  std::cout<<"COPYRIGHT"<<std::endl;
  std::cout<<"        "<<"Copyright (C) 2020 CNRS and LIRIS' Establishments (France). "
           <<"License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>"
           <<std::endl
           <<"        This is free software: you are free to change and redistribute it under certain conditions. "
           <<"There is NO WARRANTY, to the extent permitted by law."
           <<std::endl<<std::endl;
  // Reporting bugs
  std::cout<<"REPORTING BUGS"<<std::endl;
  std::cout<<"        "<<"Email bug reports to Guillaume Damiand ⟨guillaume.damiand@liris.cnrs.fr⟩."
           <<std::endl<<std::endl;
  exit(EXIT_FAILURE);
}
////////////////////////////////////////////////////////////////////////////////
[[ noreturn ]] void error_command_line(int argc, char** argv, const char* msg)
{
  std::cout<<"ERROR: "<<msg<<std::endl;
  usage(argc, argv);
}
////////////////////////////////////////////////////////////////////////////////
void process_command_line(int argc, char** argv,
                          bool& t1,
                          std::string& filename,
                          std::size_t& nbs)
{
  t1=false;
  filename="";
  nbs=10000;

  bool helprequired=false;
  std::string arg;

  for (int i=1; i<argc; ++i)
  {
    arg=std::string(argv[i]);
    if (arg==std::string("-h") || arg==std::string("--help") || arg==std::string("-?"))
    { helprequired=true; }
    else if (arg==std::string("-t1"))
    {
      t1=true;
      if (argc-1-i<1)
      { error_command_line(argc, argv, "Error: no filename after -t1 option."); }
      filename=std::string(argv[++i]);
    }
   else if (arg==std::string("-nbs"))
    {
      if (argc-1-i<1)
      { error_command_line(argc, argv, "Error: no number after -nbs option."); }
      nbs=std::stoi(std::string(argv[++i]));
    }
    else if (arg[0]=='-')
    {
      std::cout<<"Unknown option "<<arg<<std::endl;
      helprequired=true;
    }
  }

  if (!t1) { std::cout<<"ERROR: missing file name."<<std::endl; }
  if (helprequired || !t1) { usage(argc, argv); }
}
///////////////////////////////////////////////////////////////////////////////
void process_file(const std::string& filename,  std::size_t nbs)
{
  My_timer timer, global_timer, savetimer;

  global_timer.reset();
  global_timer.start();

  std::vector<double> times(4,0);

  typedef My_visitor<My_arr_segment_traits_2<EPECK>> Visitor;
  My_arr_segment_traits_2<EPECK> m_traits;

  double strip_xmin=0, strip_xmax=0, strip_ymin=0, strip_ymax=0, xmax=0;
  bool first_strip=true;
  std::list<ISegment> all_segments;
  Read_from_file<EPICK> reader(filename);
  std::size_t strip_id=0;
  std::size_t id=1; // segment id
  while (reader.ok())
  {
    // 1) Load new segments
    timer.reset(); timer.start();
    for (std::size_t i=0; reader.ok() && i<nbs; ++i)
    {
      EPICK::Segment_2 s=reader.next_segment();
      if (first_strip && all_segments.empty())
      { strip_xmin=s.source().x()-1.; xmax=s.target().x(); }
      else { xmax=std::max(xmax, s.target().x()); }
      strip_xmax=s.source().x();

      // Note that we don't need ymin and ymax !
      if (all_segments.empty())
      { strip_ymin=s.source().y(); strip_ymax=strip_ymin;}
      else
      {
        if (s.source().y()<strip_ymin) { strip_ymin=s.source().y(); }
        if (s.source().y()>strip_ymax) { strip_ymax=s.source().y(); }
      }
      if (s.target().y()<strip_ymin) { strip_ymin=s.target().y(); }
      if (s.target().y()>strip_ymax) { strip_ymax=s.target().y(); }

      all_segments.push_back(s);
    }
    if (!reader.ok())
    { strip_xmax=xmax+1; }
    timer.stop();
    times[0]+=timer.time();

    // 2) Now all_segments contains all the segments of the current strip;
    //    compute the partial hds of this strip.
    timer.reset(); timer.start();
    {
      Partial_hds phds(strip_xmin, strip_ymin, strip_xmax, strip_ymax,
                       (first_strip?0:(!reader.ok()?2:1)), 3);
      Visitor visitor(phds);
      My_surface_sweep_2<Visitor> surface_sweep(&m_traits, &visitor);
      visitor.sweep(all_segments, false, id);
      phds.display_info();

      // 3) Save the partial hds
      savetimer.reset(); savetimer.start();
      std::ofstream outputfile(std::string("strip-")+std::to_string(strip_id++)+".save");
      if (outputfile)
      { outputfile<<phds; }
      outputfile.close();
      savetimer.stop();
      times[2]+=savetimer.time();
    }
    timer.stop();
    times[1]+=(timer.time()-savetimer.time()); // time to compute partial hds without
    // including the save time; and including the de-allocation of the phds
    // (reason of the block)

    // 4) Remove all segments no more concerned by the new strip
    timer.reset(); timer.start();
    first_strip=false;
    strip_xmin=strip_xmax; // xmin of the new strip is xmax of the previous strip
    while (!all_segments.empty() &&
           (all_segments.begin())->target().x()<strip_xmin)
    {
      all_segments.pop_front();
      ++id; // id is incremented to keep global id the same
    }
    timer.stop();
    times[3]+=timer.time();
  }

  global_timer.stop();

  print_txt_with_endl("[TIME]: to load segments ", times[0]);
  print_txt_with_endl("[TIME]: to compute all partial HDS ", times[1]);
  print_txt_with_endl("[TIME]: to save partial HDS ", times[2]);
  print_txt_with_endl("[TIME]: to update segments ", times[3]);
  print_txt_with_endl("[GLOBAL]: time to compute all local arrangements ",
                      global_timer.time());
  print_txt_with_endl("[GLOBAL]: time to compute all local arrangements without save/load times ",
                      times[1]+times[3]);
}
///////////////////////////////////////////////////////////////////////////////
int main(int argc, char** argv)
{
  std::string filename;
  bool t1;
  std::size_t nbs;

  process_command_line(argc, argv, t1, filename, nbs);
  process_file(filename, nbs);

  return EXIT_SUCCESS;
}
///////////////////////////////////////////////////////////////////////////////
