import argparse
import numpy as np
import matplotlib.pyplot as plt
import os
import re

## Le Thread.log c'est le resultat de
#     cat toto.log | grep "\[Threa" >! toto-thread.log 


with open("toto-thread.log") as file:

  for line in file:
    p = re.compile( 'time=\d+.\d+' )
    alltimings = p.findall( line )
    print(len(alltimings))
    allT = []
    for athread in alltimings:
        p = re.compile( '\d+.\d+' )
        t = p.findall( athread )
        allT = allT + t
    print(allT)
