// compute_arrangement: a new method to compute arrangement of segments.
// Copyright (C) 2020 CNRS and LIRIS' Establishments (France).
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
// Author(s)     : Guillaume Damiand <guillaume.damiand@liris.cnrs.fr>
//

#ifndef MY_SURFACE_SWEEP_H
#define MY_SURFACE_SWEEP_H

#include <CGAL/Surface_sweep_2.h>

template <typename Visitor_>
class My_surface_sweep_2:
  CGAL::Surface_sweep_2::Surface_sweep_2<Visitor_>
{
public:
  typedef My_surface_sweep_2<Visitor_> Self;
  using Base=CGAL::Surface_sweep_2::Surface_sweep_2<Visitor_>;

  using Geometry_traits_2=typename Base::Geometry_traits_2;
  using Point_2=typename Base::Point_2;
  using Event_queue_iterator=typename Base::Event_queue_iterator;
  using Event_subcurve_iterator=typename Base::Event_subcurve_iterator;
  using Subcurve=typename Base::Subcurve;

  My_surface_sweep_2(Visitor_* visitor) : Base(visitor) {}
  My_surface_sweep_2(const Geometry_traits_2* traits, Visitor_* visitor) :
    Base(traits, visitor) {}

  template <typename CurveInputIterator>
  void sweep_until(CurveInputIterator curves_begin,
                   CurveInputIterator curves_end,
                   const Point_2& p)
  {
    this->m_visitor->before_sweep();
    this->_init_sweep(curves_begin, curves_end);
    //m_visitor->after_init();
    this->_sweep_until(p);
    this->_complete_sweep();
    this->m_visitor->after_sweep();
  }

protected:
  void _sweep_until(const Point_2& p)
  {
    // Looping over the events in the queue.
    Event_queue_iterator eventIter = this->m_queue->begin();

    std::size_t number_of_strips=this->m_visitor->number_of_strips();
    std::size_t strip_id=this->m_visitor->strip_id();
    bool before_strip=true;
    bool after_strip=false;

    if (strip_id==0)
    {
      before_strip=false;
      this->m_visitor->enter_in_strip();
    }

    while (eventIter != this->m_queue->end()) {
      // Get the next event from the queue.
      this->m_currentEvent = *eventIter;

      if (number_of_strips>1)
      {
        if (before_strip)
        {
          before_strip=(CGAL::compare_x(this->m_currentEvent->point(),
                                       this->m_visitor->pmin())==CGAL::SMALLER);
          if (!before_strip)
          { this->m_visitor->enter_in_strip(); }
        }
        if (!after_strip && strip_id<number_of_strips-1)
        {
          after_strip=(CGAL::compare_x(this->m_currentEvent->point(),
                                      this->m_visitor->pmax())!=CGAL::SMALLER);
          if (after_strip) { this->m_visitor->end_of_strip(); }
        }
      }

      if (!after_strip)
      {
        this->_handle_left_curves();
        this->_handle_right_curves();
      }

      if (this->m_visitor->after_handle_event(this->m_currentEvent,
                                              this->m_status_line_insert_hint,
                                              this->m_is_event_on_above))
      {
        this->deallocate_event(this->m_currentEvent);
      }

      this->m_queue->erase(eventIter);
      eventIter = this->m_queue->begin();
    }

    this->m_statusLine.clear();
  }
};

#endif // MY_SURFACE_SWEEP_H
