// Copyright (c) 2005,2006,2007,2009,2010,2011 Tel-Aviv University (Israel).
// All rights reserved.
//
// This file is part of CGAL (www.cgal.org).
//
// $URL$
// $Id$
// SPDX-License-Identifier: GPL-3.0-or-later OR LicenseRef-Commercial
//
// Author(s): Baruch Zukerman <baruchzu@post.tau.ac.il>
//            Efi Fogel       <efif@post.tau.ac.il>
//            (based on old version by Tali Zvi)

#ifndef CGAL_MY_VISITOR_H
#define CGAL_MY_VISITOR_H

/*! \file
 *
 * Definition of a surface-sweep visitor that reports all maximal x-monotone
 * non-intersecting x-monotone curves induced by a set of input curves.
 */

#include <vector>

#include <CGAL/Surface_sweep_2/Default_visitor.h>
#include <CGAL/Surface_sweep_2/Surface_sweep_2_utils.h>
#include "Hds.h"
#include "My_surface_sweep.h"
#include "My_event.h"
#include "My_construction_subcurve.h"
#include "Partial_hds.h"

/*! \class Subcurves_visitor
 *
 * A simple sweep-line visitor that reports all maximal x-monotone
 * non-intersecting x-monotone curves induced by a set of input curves. Used by
 * compute_subcurves().
 */
  template <typename GeometryTraits_2,
            typename Allocator_ = CGAL_ALLOCATOR(int)>
class My_visitor :
    public CGAL::Surface_sweep_2::Default_visitor
               <My_visitor<GeometryTraits_2, Allocator_>,
                GeometryTraits_2,
                Allocator_,
                My_event<GeometryTraits_2, Allocator_>,
                My_construction_subcurve<GeometryTraits_2,
                                         My_event<GeometryTraits_2, Allocator_>,
                                         Allocator_>
                >
{
public:
  typedef GeometryTraits_2                              Geometry_traits_2;
  typedef Allocator_                                    Allocator;

private:
  typedef Geometry_traits_2                        Gt2;
  typedef My_visitor<Gt2, Allocator>               Self;
  typedef CGAL::Surface_sweep_2::Default_visitor
                        <Self, Gt2, Allocator,
                         My_event<GeometryTraits_2, Allocator_>,
                         My_construction_subcurve<GeometryTraits_2,
                                                  My_event<GeometryTraits_2, Allocator_>,
                                                  Allocator_>> Base;

public:
  typedef typename Gt2::X_monotone_curve_2              X_monotone_curve_2;
  typedef typename Gt2::Point_2                         Point_2;

  typedef typename Base::Event                          Event;
  typedef typename Base::Subcurve                       Subcurve;

  typedef typename Subcurve::Status_line_iterator       Status_line_iterator;

protected:
  // Data members:
  Partial_hds& m_partial_hds;
  bool m_before_strip, m_after_strip;
  std::size_t m_nb_intersections;

public:
  My_visitor(Partial_hds& a_partial_hds) :
    m_partial_hds(a_partial_hds),
    m_before_strip(true),
    m_after_strip(false),
    m_nb_intersections(0)
  {}

  template<typename Container>
  void sweep(const Container& all_segments, bool crop=false, std::size_t id=1)
  {
    m_partial_hds.m_nb_input_segments=0;
    std::vector<X_monotone_curve_2> curves_vec;
    if (m_partial_hds.number_of_strips()==1)
    { curves_vec.reserve(all_segments.size()); }
    else
    {
      curves_vec.reserve((2*all_segments.size())/(m_partial_hds.number_of_strips()));
    }

    bool concerned=false, cropx1, cropx2;
    EPECK::Line_2 line;
    double val;
    ECoord x1, y1;
    ECoord x2, y2;
    auto it=all_segments.begin();
    for (; it!=all_segments.end(); ++id, ++it)
    {
      if (it->source()!=it->target())
      {
        concerned=false;

        if (it->source().x()<m_partial_hds.m_minx)
        {
          if (it->target().x()>=m_partial_hds.m_minx)
          { concerned=true; }
        }
        else
        {
          if (it->source().x()<m_partial_hds.m_maxx)
          { concerned=true; }
        }

        if (concerned)
        {
          if (crop)
          {
            if (it->source().x()<m_partial_hds.m_minx) { cropx1=true; }
            else { cropx1=false; }
            if (it->target().x()>m_partial_hds.m_maxx) { cropx2=true; }
            else { cropx2=false; }

            if (cropx1 || cropx2)
            {
              line=EPECK::Line_2(EPoint(it->source().x(), it->source().y()),
                                 EPoint(it->target().x(), it->target().y()));
              if (cropx1)
              {
                val=m_partial_hds.m_minx-
                    ((m_partial_hds.m_maxx-m_partial_hds.m_minx)/10000.);
                if (val<it->source().x())
                { cropx1=false; }
                else
                { x1=val; y1=line.y_at_x(x1); } // Exact coordinates
              }
              if (cropx2)
              { x2=m_partial_hds.m_maxx; y2=line.y_at_x(x2); }
            }

            if (!cropx1)
            { x1=it->source().x(); y1=it->source().y(); }
            if (!cropx2)
            { x2=it->target().x(); y2=it->target().y(); }
            curves_vec.push_back(X_monotone_curve_2(ESegment(EPoint(x1,y1),
                                                             EPoint(x2, y2)),
                                                    id));
          }
          else
          {
            curves_vec.push_back(X_monotone_curve_2
                                 (ESegment(EPoint(it->source().x(), it->source().y()),
                                           EPoint(it->target().x(), it->target().y())),
                                  id));
          }
          ++m_partial_hds.m_nb_input_segments;
        }
      }
    }

    // 2) Perform the sweep.
    My_surface_sweep_2<Self>* sl = reinterpret_cast<My_surface_sweep_2<Self>*>
        (this->surface_sweep());
    sl->sweep_until(curves_vec.begin(), curves_vec.end(),
                    m_partial_hds.m_max);
  }

  void enter_in_strip()
  { m_before_strip=false; }

  void end_of_strip()
  { m_after_strip=true; }

  HDS& hds()
  { return m_partial_hds.hds(); }

  const EPoint& pmin() const
  { return m_partial_hds.pmin(); }
  const EPoint& pmax() const
  { return m_partial_hds.pmax(); }

  std::size_t strip_id() const
  { return m_partial_hds.strip_id(); }
  std::size_t number_of_strips() const
  { return m_partial_hds.number_of_strips(); }

  std::size_t number_of_intersections() const
  { return m_nb_intersections; }

  // Before to process the given event in the sweep line
  void before_handle_event(Event* event)
  {
    assert(event->is_closed());
    assert(event->vertex_handle()==nullptr);

    if (m_before_strip)
    { event->set_before_strip(); return; }

    if (m_after_strip) return;

    assert(m_partial_hds.point_in_strip(event->point()));
    event->set_vertex_handle(hds().create_vertex(event->point()));

    /* print_txt_with_endl("[strip", m_partial_hds.strip_id(),
                        "] Create inner point for [",event->point()); */
  }

  // After having proceed the given even in the sweep line
   bool after_handle_event(Event* event,
                           Status_line_iterator iter,
                           bool /*is_event_on_above*/)
  {
     // print_txt_with_endl("after_handle_event ", event, " ", event->point());
     Halfedge_handle prevhh=nullptr;
     if (event->vertex_handle()!=nullptr)
     { // Point in strip
       typename Event::Subcurve_reverse_iterator subcurve_rit;
       for (subcurve_rit=event->left_curves_rbegin();
            subcurve_rit!=event->left_curves_rend(); ++subcurve_rit)
       {
         hds().set_vertex(hds().opposite((*subcurve_rit)->halfedge()),
                          event->vertex_handle());
         prevhh=hds().add_segment_around_vertex
             (hds().opposite((*subcurve_rit)->halfedge()),
              event->vertex_handle(), prevhh);
         if ((*subcurve_rit)->last_event()->vertex_handle()==nullptr)
         { m_partial_hds.add_left_external_halfedge((*subcurve_rit)->halfedge()); }
       }

       if (event->is_intersection() || event->is_weak_intersection())
       { ++m_nb_intersections; }
     }
     else if (m_after_strip)
     {
       typename Event::Subcurve_reverse_iterator subcurve_rit;
       for (subcurve_rit=event->left_curves_rbegin();
            subcurve_rit!=event->left_curves_rend(); ++subcurve_rit)
       {
         if ((*subcurve_rit)->last_event()->vertex_handle()!=nullptr ||
             (*subcurve_rit)->last_event()->before_strip())
         {
           /* print_txt_with_endl("right_segment ",
                               (*subcurve_rit)->last_event()->point(),
                               " -> ", event->point()); */
           m_partial_hds.add_right_external_halfedge
               (hds().opposite((*subcurve_rit)->halfedge()));

           if ((*subcurve_rit)->last_event()->before_strip())
           { m_partial_hds.add_left_external_halfedge((*subcurve_rit)->halfedge()); }
         }
       }

       typename Event::Subcurve_iterator subcurve_it;
       for (subcurve_it=event->right_curves_begin();
            subcurve_it!=event->right_curves_end(); ++subcurve_it)
       { (*subcurve_it)->set_last_event(event); }
     }
     else
     {
       assert(m_before_strip);
       typename Event::Subcurve_reverse_iterator subcurve_rit;
       for (subcurve_rit=event->left_curves_rbegin();
            subcurve_rit!=event->left_curves_rend(); ++subcurve_rit)
       {
         // Case of an edge entirely before the strip: ignore
         hds().erase_edge((*subcurve_rit)->halfedge());
         (*subcurve_rit)->set_halfedge(nullptr);
       }
     }

     if (!m_after_strip)
     {
       typename Event::Subcurve_iterator subcurve_it;
       for (subcurve_it=event->right_curves_begin();
            subcurve_it!=event->right_curves_end(); ++subcurve_it)
       {
         (*subcurve_it)->set_halfedge
             (hds().create_edge
              (event->vertex_handle(), (*subcurve_it)->original_segment_id()));
         if (event->vertex_handle()!=nullptr)
         {
           prevhh=hds().add_segment_around_vertex
               ((*subcurve_it)->halfedge(), event->vertex_handle(), prevhh);
         }
         (*subcurve_it)->set_last_event(event);
       }
     }

     if (event->vertex_handle()!=nullptr)
     {
       hds().end_of_vertex(event->vertex_handle(), prevhh);
       // Iter is on the status line just after event
       Halfedge_handle fatherdart=(iter!=this->status_line_end()?
             (*iter)->halfedge():nullptr);
       m_partial_hds.set_above_halfedge(event->vertex_handle(), fatherdart);
     }

     return (event->number_of_right_curves() == 0);
   }

   void found_overlap(Subcurve* sc1,
                      Subcurve* sc2,
                      Subcurve* ov_sc)
   {
     ov_sc->set_original_segment_id(std::min(sc1->original_segment_id(),
                                             sc2->original_segment_id()));
   }
};

#endif
