// compute_arrangement: a new method to compute arrangement of segments.
// Copyright (C) 2020 CNRS and LIRIS' Establishments (France).
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
// Author(s)     : Guillaume Damiand <guillaume.damiand@liris.cnrs.fr>
//

#include <cassert>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stack>
#include <tuple>
#include <thread>
#include <boost/filesystem.hpp>

#include <CGAL/assertions.h>
#include <CGAL/Surface_sweep_2.h>
#include <CGAL/Surface_sweep_2_algorithms.h>
#include <CGAL/Arr_segment_traits_2.h>
#include <CGAL/Arrangement_2.h>
#include <CGAL/Arr_consolidated_curve_data_traits_2.h>
#include <CGAL/Arr_non_caching_segment_basic_traits_2.h>

#include "CGAL_typedefs.h"
#include "Segment_readers.h"
#include "My_visitor.h"
#include "SHds.h"
#include "SHds_to_lcc.h"
#include "SHds_to_cgal_arrangement.h"
#include "memory.h"
#include "draw_shds.h"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
[[ noreturn ]] void usage(int /*argc*/, char** argv)
{
  // Name
  std::cout<<"Name"<<std::endl;
  std::cout<<"        "<<argv[0]<<" - a new method to compute arrangement of segments";
  std::cout<<std::endl<<std::endl;
  // Synopsis
  std::cout<<"SYNOPSIS"<<std::endl;
  std::cout<<"        "<<argv[0]<<" [--help|-h|-?] "
           <<"[-t1 FILENAME] [-t2 FILENAME] [-t3 FILENAME] [-cgal] [-lcc] [-traverse]"
           <<"[-nbs S] [-nbt N] [-crop] [-optimized-strips] [-xpos X1 X2 ...] "
           <<"[-export-cgal] [-draw] [-save] [-export-xfig XFIGFILE] "
           <<"[-export-xfig-sequence] [-xfig-invert-y] [-xfig-x-size S] "
           <<"[-xfig-width W1 W2 W3 W4 W5] "
           <<"[-xfig-color R1 G1 B1 R2 G2 B2 R3 G3 B3 R4 G4 B4]"
           <<std::endl<<std::endl;
  // Description
  std::cout<<"DESCRIPTION"<<std::endl;
  std::cout<<"        "<<"Compute the arrangement of segments given in the FILENAME."
           <<std::endl<<std::endl;
  // Options
  std::cout<<"        --help, -h, -?"<<std::endl
           <<"                display this help and exit."
           <<std::endl<<std::endl;
  std::cout<<"        -t1 FILENAME"
           <<std::endl
           <<"                load the text segment file, and creates the arrangement."
           <<std::endl<<std::endl;
  std::cout<<"        -t2 FILENAME"
           <<std::endl
           <<"                load the sHDS from FILENAME."
           <<std::endl<<std::endl;
  std::cout<<"        -t3 FILENAME"
           <<std::endl
           <<"                load the sequence of HDS from FILENAME, which must contains % at the position of the number."
           <<std::endl<<std::endl;
  std::cout<<"        -cgal"
           <<std::endl
           <<"                use CGAL arrangement (and not our method as default)."
           <<std::endl<<std::endl;
  std::cout<<"        -lcc"
           <<std::endl
           <<"                compute a 2D Linear cell complex from the sHDS."
           <<std::endl<<std::endl;
  std::cout<<"        -traverse"
           <<std::endl
           <<"                traverse the sHDS and compute time."
           <<std::endl<<std::endl;
  std::cout<<"        -nbs S"
           <<std::endl
           <<"                number of strips (1 by default)."
           <<std::endl<<std::endl;
  std::cout<<"        -nbt N"
           <<std::endl
           <<"                number of threads (1 by default)."
           <<std::endl<<std::endl;
  std::cout<<"        -crop"
          <<std::endl
          <<"                crop the segments to fit in its strip."
          <<std::endl<<std::endl;
  std::cout<<"        -optimized-strips"
           <<std::endl
           <<"                compute the length of each strips dynamically (by default each strip has the same size) (used only if the number of strips is > 1) (not used if option -xpos is used)."
           <<std::endl<<std::endl;
  std::cout<<"        -xpos X1 X2 ..."
          <<std::endl
          <<"                gives the x positions of the strips (we must have S-1 numbers, B being the number given after -nbs option, and this option must be used after -nbs option)."
          <<std::endl<<std::endl;
  std::cout<<"        -export-cgal"
           <<std::endl
           <<"                compute a CGAL arrangement from the sHDS."
           <<std::endl<<std::endl;
  std::cout<<"        -draw"
          <<std::endl
          <<"                draw the sHDS."
          <<std::endl<<std::endl;
  std::cout<<"        -save"
          <<std::endl
          <<"                save the sHDS."
          <<std::endl<<std::endl;
 std::cout<<"        -export-xfig XFIGFILE"
         <<std::endl
         <<"                export the sHDS into the given xfig file."
         <<std::endl<<std::endl;
 std::cout<<"        -export-xfig-sequence"
         <<std::endl
         <<"                export the strips as a sequence of xfig files."
         <<std::endl<<std::endl;
 std::cout<<"        -xfig-invert-y"
         <<std::endl
         <<"                invert y axis for the export xfig."
         <<std::endl<<std::endl;
 std::cout<<"        -xfig-x-size S"
         <<std::endl
         <<"                set the x size of the xfig file to S (default 50000.)."
         <<std::endl<<std::endl;
 std::cout<<"        -xfig-width W1 W2 W3 W4 W5"
         <<std::endl
         <<"                fix the size of: segments=W1; disks=W2; criticals=W3; borders=W4; "
         <<"space between strips=W5 (default 2 30 2 1 100) (if one width is 0, corresponding elements are not drawn)."
         <<std::endl<<std::endl;
 std::cout<<"        -xfig-color R1 G1 B1 R2 G2 B2 R3 G3 B3 R4 G4 B4"
         <<std::endl
         <<"                fix the color of: segments=R1 G1 B1; disks=R2 G2 B2; "
         <<"criticals=R3 G3 B3; borders=R4 G4 B4 (default 40 40 220  220 40 40  90 190 110  150 150 150)."
         <<std::endl<<std::endl;
 // Author
  std::cout<<"AUTHOR"<<std::endl;
  std::cout<<"        "<<"Written by Guillaume Damiand."
           <<std::endl<<std::endl;
  // Copyright
  std::cout<<"COPYRIGHT"<<std::endl;
  std::cout<<"        "<<"Copyright (C) 2020 CNRS and LIRIS' Establishments (France). "
           <<"License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>"
           <<std::endl
           <<"        This is free software: you are free to change and redistribute it under certain conditions. "
           <<"There is NO WARRANTY, to the extent permitted by law."
           <<std::endl<<std::endl;
  // Reporting bugs
  std::cout<<"REPORTING BUGS"<<std::endl;
  std::cout<<"        "<<"Email bug reports to Guillaume Damiand ⟨guillaume.damiand@liris.cnrs.fr⟩."
           <<std::endl<<std::endl;
  exit(EXIT_FAILURE);
}
////////////////////////////////////////////////////////////////////////////////
[[ noreturn ]] void error_command_line(int argc, char** argv, const char* msg)
{
  std::cout<<"ERROR: "<<msg<<std::endl;
  usage(argc, argv);
}
////////////////////////////////////////////////////////////////////////////////
void process_command_line(int argc, char** argv,
                          std::string& filename,
                          bool& t1,
                          bool &t2,
                          bool &t3,
                          bool &cgal,
                          bool &lcc,
                          bool &traverse,
                          std::size_t& nbstrips,
                          std::size_t& nbthreads,
                          bool &crop,
                          bool &optimized_strips,
                          std::vector<double>& xpos_cuts,
                          bool &export_cgal,
                          bool &draw,
                          bool &save,
                          std::string& xfig_file,
                          bool &export_xfig_sequence,
                          bool &xfig_invert_y,
                          double &xfig_x_size,
                          uint16_t& width_segments,
                          uint16_t& radius_disks,
                          uint16_t& width_criticals,
                          uint16_t& width_borders,
                          uint16_t& space_between_strips,
                          CGAL::Color& color_segments,
                          CGAL::Color& color_disks,
                          CGAL::Color& color_criticals,
                          CGAL::Color& color_borders)
 {
  t1=false;
  t2=false;
  t3=false;
  cgal=false;
  lcc=false;
  traverse=false;
  filename="";
  nbstrips=1;
  nbthreads=1;
  draw=false;
  optimized_strips=false;
  export_cgal=false;
  save=false;
  crop=false;
  xfig_file="";
  export_xfig_sequence=false;
  xpos_cuts.clear();

  width_segments=2;
  radius_disks=30;
  width_criticals=2;
  width_borders=1;
  space_between_strips=100;

  xfig_invert_y=false;
  xfig_x_size=50000;

  color_segments=CGAL::Color(40,40,220);
  color_disks=CGAL::Color(220,40,40);
  color_criticals=CGAL::Color(90,190,110),
  color_borders=CGAL::Color(150,150,150);

  bool helprequired=false;
  std::string arg;

  for (int i=1; i<argc; ++i)
  {
    arg=std::string(argv[i]);
    if (arg==std::string("-h") || arg==std::string("--help") || arg==std::string("-?"))
    { helprequired=true; }
    else if (arg==std::string("-t1"))
    {
      t1=true;
      if (argc-1-i<1)
      { error_command_line(argc, argv, "Error: no filename after -t1 option."); }
      filename=std::string(argv[++i]);
    }
    else if (arg==std::string("-t2"))
    {
      t2=true;
      if (argc-1-i<1)
      { error_command_line(argc, argv, "Error: no filename after -t2 option."); }
      filename=std::string(argv[++i]);
    }
    else if (arg==std::string("-t3"))
    {
      t3=true;
      if (argc-1-i<1)
      { error_command_line(argc, argv, "Error: no filename after -t3 option."); }
      filename=std::string(argv[++i]);
      if (filename.find("%")==std::string::npos)
      { error_command_line(argc, argv, "Error: filename after -t3 option does not contain % character."); }
    }
    else if (arg==std::string("-cgal"))
    { cgal=true; }
    else if (arg==std::string("-lcc"))
    { lcc=true; }
    else if (arg==std::string("-traverse"))
    { traverse=true; }
    else if (arg==std::string("-nbs"))
    {
      if (argc-1-i<1)
      { error_command_line(argc, argv, "Error: no number after -nbs option."); }
      nbstrips=std::stoi(std::string(argv[++i]));
    }
    else if (arg==std::string("-nbt"))
    {
      if (argc-1-i<1)
      { error_command_line(argc, argv, "Error: no number after -nbt option."); }
      nbthreads=std::stoi(std::string(argv[++i]));
    }
    else if (arg==std::string("-crop"))
    { crop=true; }
    else if (arg==std::string("-optimized-strips"))
    { optimized_strips=true; }
    else if (arg==std::string("-xpos"))
    {
      if (nbstrips<=1)
      { error_command_line(argc, argv, "Error: -xpos option can only be used after-nbs option, and with a number of strips >1."); }
      if (argc-1-i<nbstrips-1)
      { error_command_line(argc, argv, "Error: we need (numberofstrips-1) numbers -xpos option."); }

      xpos_cuts.resize(nbstrips-1);
      for (std::size_t j=0; j<nbstrips-1; ++j)
      { xpos_cuts[j]=std::stod(std::string(argv[++i])); }
    }
    else if (arg==std::string("-export-cgal"))
    { export_cgal=true; }
    else if (arg==std::string("-draw"))
    { draw=true; }
    else if (arg==std::string("-save"))
    { save=true; }
    else if (arg==std::string("-export-xfig"))
    {
      if (argc-1-i<1)
      { error_command_line(argc, argv, "Error: no number after -export-xfig option."); }
      xfig_file=std::string(argv[i+1]);
    }
    else if (arg==std::string("-export-xfig-sequence"))
    { export_xfig_sequence=true; }
    else if (arg==std::string("-xfig-invert-y"))
    { xfig_invert_y=true; }
    else if (arg==std::string("-xfig-x-size"))
    {
      if (argc-1-i<1)
      { error_command_line(argc, argv, "Error: no number after -xfig-x-size option."); }
      xfig_x_size=std::stod(std::string(argv[++i]));
    }
    else if (arg==std::string("-xfig-width"))
    {
      if (argc-1-i<5)
      { error_command_line(argc, argv, "Error: we need 5 numbers after -xfig-width option."); }
      width_segments=std::stoi(std::string(argv[++i]));
      radius_disks=std::stod(std::string(argv[++i]));
      width_criticals=std::stoi(std::string(argv[++i]));
      width_borders=std::stoi(std::string(argv[++i]));
      space_between_strips=std::stoi(std::string(argv[++i]));
    }
    else if (arg==std::string("-xfig-color"))
    {
      if (argc-1-i<12)
      { error_command_line(argc, argv, "Error: we need 12 numbers after -xfig-color option."); }
      color_segments=CGAL::Color(std::stoi(std::string(argv[++i])),
          std::stoi(std::string(argv[++i])),
          std::stoi(std::string(argv[++i])));
      color_disks=CGAL::Color(std::stoi(std::string(argv[++i])),
          std::stoi(std::string(argv[++i])),
          std::stoi(std::string(argv[++i])));
      color_criticals=CGAL::Color(std::stoi(std::string(argv[++i])),
          std::stoi(std::string(argv[++i])),
          std::stoi(std::string(argv[++i])));
      color_borders=CGAL::Color(std::stoi(std::string(argv[++i])),
          std::stoi(std::string(argv[++i])),
          std::stoi(std::string(argv[++i])));
    }
    else if (arg[0]=='-')
    {
      std::cout<<"Unknown option "<<arg<<std::endl;
      helprequired=true;
    }
  }

  if (nbthreads==0) { nbthreads=1; }
  if (!t1 && !t2 && !t3) { std::cout<<"ERROR: missing file name (either with -t1 or -t2 or -t3)."<<std::endl; }
  if (cgal && (t2 || t3))  { std::cout<<"ERROR: -t2 and -t3 option cannot be used with -cgal."<<std::endl; }
  if (helprequired || (!t1 && !t2 && !t3)) { usage(argc, argv); }
}
///////////////////////////////////////////////////////////////////////////////
void run_cgal_method(const std::string& filename)
{
  typedef CGAL::Arr_segment_traits_2<EPECK> Traits_2;
  typedef CGAL::Arrangement_2<Traits_2>  Arrangement_2;

  My_timer timer;
  timer.start();
  Arrangement_2 arr;

  My_timer load_timer;
  load_timer.start();

  std::vector<ESegment> all_segments;
  all_segments.reserve(number_of_lines(filename));
  Read_from_file<EPECK> reader(filename);
  fill_segment_array<EPECK>(reader, all_segments);

  load_timer.stop();

  CGAL::insert(arr, all_segments.begin(), all_segments.end());
  timer.stop();

  print_txt_with_endl("Nb input segments: ", all_segments.size());
  print_txt_with_endl("#halfedges=", arr.number_of_halfedges(), ", #0-cells=",
                      arr.number_of_vertices(), ", #1-cells=",
                      arr.number_of_edges(), ", #2-cells=",
                      arr.number_of_faces(), ", #finite-2-cells=",
                      arr.number_of_faces()-arr.number_of_unbounded_faces());

  std::cout<<"CURRENT RSS: "<<getCurrentRSS()<<" bytes (i.e. "<<
    double(getCurrentRSS())/double(1024*1024)<<" mega-bytes and "<<
    double(getCurrentRSS())/double(1024*1024*1024)<<" giga-bytes)"<<std::endl;

  print_txt_with_endl("[TIME]: to load segments ", load_timer.time());
  print_txt_with_endl("[GLOBAL]: time to build cgal arrangement ", timer.time());
}
///////////////////////////////////////////////////////////////////////////////
int main(int argc, char** argv)
{
  std::string filename;
  bool t1;
  bool t2;
  bool t3;
  bool cgal;
  bool lcc;
  bool traverse;
  std::size_t nbstrips;
  std::size_t nbthreads;
  bool crop;
  bool optimized_strips;
  std::vector<double> xpos_cuts;
  bool export_cgal;
  bool draw;
  bool save;
  std::string xfig_file;
  bool export_xfig_sequence;
  bool xfig_invert_y;
  double xfig_x_size;
  uint16_t width_segments;
  uint16_t radius_disks;
  uint16_t width_criticals;
  uint16_t width_borders;
  uint16_t space_between_strips;
  CGAL::Color color_segments; //(svg::Color::Transparent);
  CGAL::Color color_disks;
  CGAL::Color color_criticals;
  CGAL::Color color_borders;

  std::vector<double> times;
  process_command_line(argc, argv, filename, t1, t2, t3, cgal, lcc, traverse,
                       nbstrips, nbthreads, crop, optimized_strips,
                       xpos_cuts, export_cgal, draw,
                       save, xfig_file, export_xfig_sequence, xfig_invert_y,
                       xfig_x_size,  width_segments, radius_disks,
                       width_criticals, width_borders, space_between_strips,
                       color_segments, color_disks, color_criticals,
                       color_borders);

  if (cgal)
  {
    run_cgal_method(filename);
    exit(EXIT_SUCCESS);
  }

  SHds *shds=nullptr;
  if (t1)
  {
    shds=new SHds(filename, nbstrips, nbthreads,
                optimized_strips, times, crop,
                (xpos_cuts.size()>0?&xpos_cuts:nullptr));
  }
  else if (t2)
  {
    My_timer timer; timer.start();
    shds=new SHds;
    std::ifstream inputfile(filename);
    if (inputfile)
    { inputfile>>*shds; }
    inputfile.close();
    timer.stop();
    times.push_back(timer.time());

    timer.reset(); timer.start();
    shds->build_faces_array(); // For now faces are not saved, thus recomputed here
    timer.stop();
    times.push_back(timer.time());
  }
  else if (t3)
  {
    My_timer timer; timer.start();
    shds=new SHds;
    shds->load_streamed_files(filename);
    timer.stop();
    times.push_back(timer.time());

    timer.reset(); timer.start();
    shds->build_faces_array(); // For now faces are not saved, thus recomputed here
    timer.stop();
    times.push_back(timer.time());
   }

  shds->display_info();

  // times[0] : global time
  // times[1] : load and dispatch
  // times[2] : compute all partial HDS
  // times[3] : compute faces

  if (t1)
  {
    print_txt_with_endl("[TIME]: to load and dispatch segments ", times[1]);
    print_txt_with_endl("[TIME]: to compute all partial HDS ", times[2]);
    print_txt_with_endl("[TIME]: to compute faces ", times[3]);
    print_txt_with_endl("[GLOBAL]: time to compute the sHDS ",
                        times[0]);

    if (save)
    {
      std::ofstream outputfile(filename+"output");
      if (outputfile)
      { outputfile<<*shds; }
      outputfile.close();
    }
  }
  else
  {
    print_txt_with_endl("[TIME]: to load the sHDS ", times[0]);
    print_txt_with_endl("[TIME]: to compute faces ", times[1]);
  }

  if (traverse)
  {
    My_timer local_timer;
    local_timer.start();
    std::size_t nb=0;
    for (int i=0; i<10; ++i)
    { nb+=shds->traverse_all_faces(); }
    local_timer.stop();
    print_txt_with_endl("[TIME]: to traverse the sHDS 10 times ", local_timer.time(),
                        " (",nb/10," halfedges)");
  }

  if (xfig_file!="")
  {
    if (!export_xfig_sequence)
    {
      shds->export_to_xfig(xfig_file, !xfig_invert_y, xfig_x_size, 0,
                         color_segments, color_disks, color_criticals, color_borders,
                         width_segments, radius_disks, width_criticals,
                         width_borders, space_between_strips);
    }
    else
    {
      for (std::size_t i=0; i<shds->number_of_strips(); ++i)
      {
        boost::filesystem::path p1=xfig_file;
        boost::filesystem::path p2=std::to_string(i)+std::string(".fig");
        boost::filesystem::path p=p1.replace_extension(p2);

        shds->export_to_xfig(p.string(), !xfig_invert_y, xfig_x_size, i+1,
                           color_segments, color_disks, color_criticals, color_borders,
                           width_segments, radius_disks, width_criticals,
                           width_borders, space_between_strips);
      }
    }
  }

  if (lcc)
  {
    My_timer lcc_timer;
    lcc_timer.start();
    LCC_2 lcc;
    std::size_t nb_finite_faces=shds_to_lcc(*shds, lcc);
    lcc_timer.stop();

    lcc.display_characteristics(std::cout)<<", #finite-2-cells="<<nb_finite_faces
                                         <<(lcc.is_valid()?", valid.":", NOT VALID.")<<std::endl;
    print_txt_with_endl("[TIME]: to compute a LCC from the sHDS ", lcc_timer.time());
  }

  if (export_cgal)
  {
    typedef CGAL::Arr_segment_traits_2<EPECK> Traits_2;
    typedef CGAL::Arrangement_2<Traits_2>  Arrangement_2;
    My_timer arr_timer;
    arr_timer.start();
    Arrangement_2 arr;
    shds_to_cgal_arrangement(*shds, arr);
    arr_timer.stop();
    print_txt_with_endl("[TIME]: to export the sHDS into cgal arrangement ", arr_timer.time());
    print_txt_with_endl("[GLOBAL]: time to build cgal arrangement from the sHDS ",
                        times[0]+arr_timer.time());
  }

  if (draw)
  { draw_shds(*shds, "Viewer", false); }

  delete shds;
  return EXIT_SUCCESS;
}
///////////////////////////////////////////////////////////////////////////////
