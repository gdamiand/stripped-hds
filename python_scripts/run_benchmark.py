import argparse
import os
import subprocess
import time

from enum import Enum

from generate_data import bin_exist

class Heuristic(Enum):

    EDGE         = "edge"
    INTERSECTION = "intersection"
    SEGMENT      = "segment"
    SEGMENT_OLD  = "segment-old"

    def __str__(self):
        return self.value

    def __repr__(self):
        return str(self)

PARALLEL_ARRANGEMENT_BIN =       "./builds/release/parallel-arrangement"
# COMPUTE_BAND_BIN =               "./builds/release/tools/compute-band"

def parallel_arrangement(t1, nbt,
                         xpos=None,
                         crop=False, optimized=False, traverse=False,
                         cgal=False):

    args = [
        PARALLEL_ARRANGEMENT_BIN,
        "-t1",          t1,
        "-nbt",         str(nbt),
        "-nbs",         str(nbt)
    ]
    if xpos:
        args.append("-xpos")
        args.extend(map(str,xpos))
    if crop:
        args.append("-crop")
    if optimized:
        args.append("-optimized-bands")
    if traverse:
        args.append("-traverse")
    if cgal:
        args.append("-cgal")

    return subprocess.run(args, stdout=subprocess.PIPE)

# def compute_band(filename, nbband, heuristic):

#     args = [
#         COMPUTE_BAND_BIN,
#         filename,
#         str(nbband),
#         "--" + str(heuristic)
#     ]

#     proc = subprocess.run(args, stdout=subprocess.PIPE)

#     lines = proc.stdout.decode("utf-8").splitlines()
#     pos = lines[-1].split()[1:]
#     band_pos = [ float(e) for e in pos ]

#     return proc, band_pos

if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument("input", nargs="+",
                        help="set input, input can be a directory or list of file")

    parser.add_argument('--verbose', '-v', action='count', default=0,
                        help="set verbosity level")
    parser.add_argument("--log", "-l", default="log.txt",
                        help="set log filename, default is log.txt")

    config = parser.add_argument_group("benchmark configuration")

    config.add_argument("--thread", "-t", required=True, type=int, nargs="+",
                        help="set number of threads")
    config.add_argument("--repeat", "-r", required=True, type=int, default=5,
                        help="set number of repeatition per data")
    config.add_argument("--nocgal", action="store_true",
                        help="disable benchmark with cgal method")

    optimi = parser.add_argument_group("optimization configuration")

    optimi.add_argument("--optimized", action="store_true",
                        help="use optimized method")
    optimi.add_argument("--crop", action="store_true",
                        help="use crop method")
    optimi.add_argument("--heuristic", type=Heuristic,
                        choices=list(Heuristic),
                        help="set heuristic used to compute band for each thread")

    binary = parser.add_argument_group("binaries configuration")

    binary.add_argument("--arr-bin", default=PARALLEL_ARRANGEMENT_BIN,
                        help="set 'parallel-arrangement' binary path, default value is set line 9")
    # binary.add_argument("--band-bin", default=COMPUTE_BAND_BIN,
    #                     help="set 'compute-band' binary path, default value is set line 11")

    args = parser.parse_args()

    PARALLEL_ARRANGEMENT_BIN = args.arr_bin
    # COMPUTE_BAND_BIN = args.band_bin

    if not bin_exist(PARALLEL_ARRANGEMENT_BIN):
        raise NameError("'" + PARALLEL_ARRANGEMENT_BIN + "' parallel-arrangement path does not exist. "
                        "Set variable 'PARALLEL_ARRANGEMENT_BIN' line 23 to a correct path.")
    # if not bin_exist(COMPUTE_BAND_BIN):
    #     raise NameError("'" + COMPUTE_BAND_BIN + "' compute-band path does not exist. "
    #                     "Set variable 'COMPUTE_BAND_BIN' line 24 to a correct path.")

    if len(args.input) == 1 and os.path.isdir(args.input[0]):
        dir = args.input[0]
        args.input = [ os.path.join(dir, f) for f in os.listdir(dir) ]

    iter = 0
    total_iter = (len(args.thread) + (0 if args.nocgal else 1)) * len(args.input) * args.repeat

    def advance_and_print():
        global iter
        iter += 1
        if args.verbose > 1:
            print("progress: %d / %d" % (iter, total_iter))

    log_separator = ("#" * 80 + "\n").encode()

    start = time.time()

    with open(args.log, "wb") as log_file:

        for input in args.input:

            band_xpos = { t:None for t in args.thread }

            # if args.heuristic:
            #     for thread in args.thread:
            #         if thread != 1:
            #             _, pos = compute_band(input, thread, args.heuristic)
            #             band_xpos[thread] = pos

            for j in range(args.repeat):

                if not args.nocgal:
                    proc = parallel_arrangement(input, 1,
                                                cgal=True,
                                                traverse=True)
                    log_file.write(log_separator)
                    log_file.write(("# \"%s\" repeat %d thread cgal\n" % (input, j)).encode())
                    log_file.write(proc.stdout)

                    advance_and_print()

                for thread in args.thread:
                    proc = parallel_arrangement(input, thread,
                                                crop=args.crop,
                                                optimized=args.optimized,
                                                xpos=band_xpos[thread],
                                                traverse=True)
                    log_file.write(log_separator)
                    log_file.write(("# \"%s\" repeat %d thread %d\n" % (input, j, thread)).encode())
                    log_file.write(proc.stdout)

                    advance_and_print()

    stop = time.time()
    if args.verbose:
        print("ellapsed time:", stop - start, "sec")
