// compute_arrangement: a new method to compute arrangement of segments.
// Copyright (C) 2020 CNRS and LIRIS' Establishments (France).
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
// Author(s)     : Guillaume Damiand <guillaume.damiand@liris.cnrs.fr>
//

#ifndef PARALLEL_ARRANGEMENT_TO_LCC_H
#define PARALLEL_ARRANGEMENT_TO_LCC_H

#include "SHds.h"
#include "CGAL_typedefs.h"
#include <tbb/parallel_for.h>
#include <tbb/blocked_range.h>
#include <unordered_map>

template<typename LCC>
class SHDS_to_LCC
{
public:
  typedef std::unordered_map<Halfedge_handle, typename LCC::Dart_handle> Assoc;

  SHDS_to_LCC(SHds& ashds, LCC& alcc): m_shds(ashds), m_lcc(alcc)
  {}

  std::size_t shds_to_lcc()
  {
    m_lcc.clear();
    create_darts();
    set_beta_and_vertices_links();
    return build_faces_array();
  }

  typename LCC::Dart_handle halfedge_to_dart(Halfedge_handle hh)
  {
    return assoc[hh];
  }

  void create_darts()
  {
    for (std::size_t i=0; i<m_shds.number_of_strips(); ++i)
    {
      for (auto it=m_shds.hds(i).halfedges().begin(),
           itend=m_shds.hds(i).halfedges().end(); it!=itend; ++it)
      {
        if (!m_shds.partial_hds(i).is_external(it))
        { assoc[it]=m_lcc.create_dart(); }
      }
    }
  }

  void set_beta_and_vertices_links()
  {
    tbb::parallel_for(tbb::blocked_range<std::size_t>(0, m_shds.number_of_strips()),
                      [&](tbb::blocked_range<std::size_t> r)
    {
      for (std::size_t i=r.begin(); i<r.end(); ++i)
      {
        Global_halfedge hh;
        for (auto it=m_shds.hds(i).halfedges().begin(),
             itend=m_shds.hds(i).halfedges().end(); it!=itend; ++it)
        {
          if (!m_shds.partial_hds(i).is_external(it))
          { // First halfedge is it
            hh=m_shds.opposite(Global_halfedge(i, it)); // Opposite halfedge, non external
            if (it<hh.halfedge())
            { m_lcc.template basic_link_beta_for_involution<2>
                  (halfedge_to_dart(it),
                   halfedge_to_dart(hh.halfedge())); }
            m_lcc.template basic_link_beta_1
                (halfedge_to_dart(it),
                 halfedge_to_dart(hh.halfedge()->next_around_vertex()));
          }
        }
      }
    }
    );


    // Last step not in parallel to keep vertices in the same order.
    typename LCC::Dart_handle above=nullptr;
    for (std::size_t i=0; i<m_shds.number_of_strips(); ++i)
    {
      Global_halfedge hh;
      for (auto it=m_shds.hds(i).vertices().begin(),
           itend=m_shds.hds(i).vertices().end(); it!=itend; ++it)
      {
        above=nullptr;
        if (it->above_halfedge()!=nullptr)
        {
          hh=Global_halfedge(i, it->above_halfedge());
          m_shds.move_to_non_external(hh);
          above=halfedge_to_dart(hh.halfedge());
          assert(above!=nullptr);
        }
        m_lcc.set_vertex_attribute(halfedge_to_dart(it->first_halfedge()),
                                 m_lcc.create_vertex_attribute(m_shds.hds(i).point(it), above));
      }
    }
  }

  // Build the faces of the given lcc (and the father/son relations between holes)
  // Return the number of finite faces. TODO copy the face array of m_shds
  // instead of re-compute it.
  std::size_t build_faces_array()
  {
    // display_info();

    auto m_root=m_lcc.template create_attribute<2>();
    m_root->info().m_finite=true; // Infinite face but considered as finite
    // to ensure the property: each infinite face is included into a finite one.

    std::size_t finite_faces=0;
    auto treated=m_lcc.get_new_mark();
    typename LCC::template Attribute_handle<2>::type newf, father;
    Dart_handle dh, initdh, firstdart, fatherdart;
    std::stack<Dart_handle> totreat;

    // Iterate through all inner points, ordered from left to right
    for (std::size_t i=0; i<m_shds.number_of_strips(); ++i)
    {
      for (auto it=m_shds.hds(i).vertices().begin(),
           itend=m_shds.hds(i).vertices().end(); it!=itend; ++it)
      {
        if (!m_shds.partial_hds(i).is_external(it->first_halfedge()) &&
            !m_lcc.is_marked(halfedge_to_dart(it->first_halfedge()),
                           treated))
        { // first_halfedge belongs necessarily to one infinite face.
          firstdart=halfedge_to_dart(it->first_halfedge());
          assert(m_lcc.is_dart_used(firstdart));

          newf=m_lcc.template create_attribute<2>();
          newf->info().m_finite=false; // Infinite face

          // std::size_t nbdarts=0;
          initdh=dh=firstdart;
          do
          {
            // ++nbdarts;
            m_lcc.mark(dh, treated);
            m_lcc.template set_dart_attribute<2>(dh, newf);
            if (!m_lcc.is_marked(m_lcc.template beta<2>(dh), treated))
            { totreat.push(m_lcc.template beta<2>(dh)); }
            dh=m_lcc.template beta<1>(dh);
          }
          while(dh!=initdh);

          /* print_txt_with_endl("[Infinite Face ",
                              m_lcc.template attributes<2>().index(newf), "]: ",
                              "first segment: ",
                              m_lcc.point(firstdart), " , ",
                              m_lcc.point(m_lcc.other_extremity(firstdart)),
                              ";  #darts=", nbdarts); */
          fatherdart=m_lcc.template info<0>(firstdart).m_dart_above;
          father=nullptr;
          if (fatherdart==nullptr)
          { father=m_root; }
          else
          {
            assert(m_lcc.is_dart_used(fatherdart));
            assert(m_lcc.template attribute<2>(fatherdart)!=nullptr);
            assert(m_lcc.point(fatherdart)<
                   m_lcc.point(m_lcc.other_extremity(fatherdart)));

            /*print_txt_with_endl("father_of_first_halfedge: ",
                                m_lcc.point(fatherdart), " , ",
                                m_lcc.point(m_lcc.other_extremity(fatherdart))); */

            if (m_lcc.template info<2>(fatherdart).m_finite)
            { // Father is the dart of the finite face containing this hole
              father=m_lcc.template attribute<2>(fatherdart);
            }
            else
            { // Father is a dart of another hole, inside the same finite face
              // This hole was already considered before.
              assert(m_lcc.template info<2>(fatherdart).m_father!=nullptr);
              father=m_lcc.template info<2>(fatherdart).m_father;
            }
          }
          /* print_txt_with_endl(" Father of face ",
                              m_lcc.template attributes<2>().index(newf),
                              " -> ", m_lcc.template attributes<2>().index(father));
          */
          assert(father->info().m_finite);
          newf->info().m_father=father;
          father->info().m_son.push_back(newf);

          // Now we iterate through the cc containing it->first_halfedge().
          // Each new face is necessarily a finite one.
          while(!totreat.empty())
          {
            initdh=totreat.top(); totreat.pop();
            if (!m_lcc.is_marked(initdh, treated))
            {
              newf=m_lcc.template create_attribute<2>();
              newf->info().m_finite=true; // Finite face
              ++finite_faces;

              // nbdarts=0;
              dh=initdh;
              do
              {
                // ++nbdarts;
                m_lcc.mark(dh, treated);
                m_lcc.template set_dart_attribute<2>(dh, newf);
                if (!m_lcc.is_marked(m_lcc.template beta<2>(dh), treated))
                { totreat.push(m_lcc.template beta<2>(dh)); }
                dh=m_lcc.template beta<1>(dh);
              }
              while(dh!=initdh);

              /* print_txt_with_endl("[Finite Face ",
                                  m_lcc.template attributes<2>().index(newf), "]: ",
                                  "father: ",
                                  m_lcc.template attributes<2>().index(father),
                                  "  first segment: ",
                                  m_lcc.point(initdh), " , ",
                                  m_lcc.point(m_lcc.other_extremity(initdh)),
                                  ";  #darts=", nbdarts); */
            }
          }
        }
      }
    }
    assert(m_lcc.is_whole_map_marked(treated));
    m_lcc.free_mark(treated);

    return finite_faces;
  }

protected:
  SHds& m_shds;
  LCC&  m_lcc;
  Assoc assoc;
};


// Build a global lcc from a parallel arrangement.
// @Return the number of finite faces.
template<typename LCC>
std::size_t shds_to_lcc(SHds& ashds, LCC& alcc)
{
  SHDS_to_LCC<LCC> shds_to_lcc(ashds, alcc);
  return shds_to_lcc.shds_to_lcc();
}

#endif // SHDS_TO_LCC_H
