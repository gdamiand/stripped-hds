// compute_arrangement: a new method to compute arrangement of segments.
// Copyright (C) 2020 CNRS and LIRIS' Establishments (France).
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
// Author(s)     : Guillaume Damiand <guillaume.damiand@liris.cnrs.fr>
//

#ifndef DRAW_SHDS_H
#define DRAW_SHDS_H

#include <CGAL/Qt/Basic_viewer_qt.h>

#ifdef CGAL_USE_BASIC_VIEWER

#include <CGAL/Random.h>
#include <CGAL/Qt/Basic_viewer_qt.h>
#include "SHds.h"

// Viewer class for sHDS
class MySimpleSHDSViewerQt : public CGAL::Basic_viewer_qt
{
  typedef CGAL::Basic_viewer_qt Base;
  typedef typename Base::Local_point Point;

public:
  /// Construct the viewer.
  /// @param ashds the sHDS to view
  /// @param title the title of the window
  /// @param anofaces if true, do not draw faces (faces are not computed; this can be
  ///        usefull for very big object where this time could be long)
  MySimpleSHDSViewerQt(QWidget* parent,
                       SHds& ashds,
                       const char* title="Basic Viewer for sHDS",
                       bool anofaces=false) :
    // First draw: vertices; edges, faces; multi-color; inverse normal
    Base(parent, title, true, true, true, false, false),
    m_shds(ashds),
    m_nofaces(anofaces),
    m_random_face_color(true),
    m_num_face(0),
    m_draw_grid(true),
    m_cur_strip(0),
    m_color_points(10, 10, 255),
    m_color_grid(220, 220, 220),
    m_color_selected_he(10,255,10),
    m_color_above_links(85,210,180)
  {
    compute_elements();
  }

protected:
  void draw_grid()
  {
    for (std::size_t i=0; i<m_shds.number_of_strips(); ++i)
    {
      const Partial_hds& la=m_shds.partial_hds(i);
      add_segment(EPoint(la.xmin(), la.ymin()),
                  EPoint(la.xmax(), la.ymin()), m_color_grid);

      add_segment(EPoint(la.xmin(), la.ymax()),
                  EPoint(la.xmax(), la.ymax()), m_color_grid);

      add_segment(EPoint(la.xmin(), la.ymin()),
                  EPoint(la.xmin(), la.ymax()), m_color_grid);
    }

    const Partial_hds& la=m_shds.
      partial_hds(m_shds.number_of_strips()-1);
    add_segment(EPoint(la.xmax(), la.ymin()),
                EPoint(la.xmax(), la.ymax()), m_color_grid);
  }

  void compute_face(const Global_halfedge& dh)
  {
    if (m_nofaces) return;
    assert (m_shds.face(dh)->finite());

    // We fill only closed faces.
    Global_halfedge cur=dh;
    Global_halfedge min=dh;
    do
    {
      if (cur<min) min=cur;
      m_shds.move_to_next(cur);
    }
    while(cur!=dh);

    if (m_random_face_color)
    {
      CGAL::Random random(m_shds.hds(min).halfedges().index(min.halfedge()));
      CGAL::Color c=get_random_color(random);
      face_begin(c);
    }
    else
    { face_begin(); }

    cur=dh;
    do
    {
      add_point_in_face(m_shds.point(cur));
      m_shds.move_to_next(cur);
    }
    while(cur!=dh);

    for (std::size_t i=0; i<m_shds.face(dh)->sons().size(); ++i)
    {
      assert(!m_shds.face(dh)->sons()[i]->finite());
      add_point_in_face(m_shds.point(dh));
      cur=Global_halfedge(m_shds.face(dh)->sons()[i]->strip_id(),
                            m_shds.face(dh)->sons()[i]->first_halfedge());
      min=cur;
      do
      {
        add_point_in_face(m_shds.point(cur));
        if (m_num_face!=0)
        { compute_edge(cur); }
        m_shds.move_to_next(cur);
      }
      while(cur!=min);
      add_point_in_face(m_shds.point(cur));
    }
    face_end();
  }

  void compute_edge(const Global_halfedge& phe)
  {
    const auto& p1 = m_shds.point(phe);
    Global_halfedge d2=m_shds.opposite(phe);

    if (m_cur_she.halfedge()!=nullptr && (phe==m_cur_she || m_cur_she==d2))
    {
      add_segment(p1, m_shds.point(d2), m_color_selected_he);
      return;
    }

    bool concerned=(m_cur_strip==0?true:false);
    if (m_cur_strip!=0)
    {
      CGAL::Comparison_result c1=CGAL::compare_x(p1, m_shds.partial_hds(m_cur_strip-1).m_min);
      CGAL::Comparison_result c2=CGAL::compare_x(p1, m_shds.partial_hds(m_cur_strip-1).m_max);
      if (c1!=CGAL::SMALLER && c2==CGAL::SMALLER) // Point in strip
      { concerned=true; }
      else
      {
        CGAL::Comparison_result c3=CGAL::compare_x(m_shds.point(d2), m_shds.partial_hds(m_cur_strip-1).m_min);
        CGAL::Comparison_result c4=CGAL::compare_x(m_shds.point(d2), m_shds.partial_hds(m_cur_strip-1).m_max);
        if (c3!=CGAL::SMALLER && c4==CGAL::SMALLER) // Point in strip
        { concerned=true; }
        else
        { concerned=(c1==CGAL::SMALLER && c4!=CGAL::SMALLER); }
      }
    }
    if (!concerned) return;

    add_segment(p1, m_shds.point(d2));
  }

  void compute_vertex(const Global_halfedge& phe)
  {
    if (m_cur_she.halfedge()!=nullptr && m_shds.vertex(phe)==m_shds.vertex(m_cur_she))
    {
      add_point(m_shds.point(phe), m_color_selected_he);

      Global_halfedge above(phe.strip_id(), m_shds.vertex(phe)->above_halfedge());
      if (above.halfedge()!=nullptr)
      {
        m_shds.move_to_non_external(above);
        add_segment(m_shds.point(phe), m_shds.point(above), m_color_above_links);
        add_segment(m_shds.point(phe), m_shds.point(m_shds.opposite(above)),
                    m_color_above_links);
      }
      return;
    }

    bool concerned=(m_cur_strip==0?true:false);
    if (m_cur_strip!=0)
    {
      CGAL::Comparison_result c1=CGAL::compare_x
          (m_shds.point(phe), m_shds.partial_hds(m_cur_strip-1).m_min);
      CGAL::Comparison_result c2=CGAL::compare_x
          (m_shds.point(phe), m_shds.partial_hds(m_cur_strip-1).m_max);
      if (c1!=CGAL::SMALLER && c2==CGAL::SMALLER) // Point in strip
      { concerned=true; }
    }
    if (!concerned) return;

    add_point(m_shds.point(phe));
  }

  void compute_elements()
  {
    clear();

    if (m_draw_grid)
    { draw_grid(); }

    typename SHds::size_type markedges    = m_shds.get_new_mark();
    typename SHds::size_type markvertices = m_shds.get_new_mark();

    std::size_t n=0;
    auto it=m_shds.faces().begin(), itend=m_shds.faces().end();
    for (++it; it!=itend; ++it ) // We need to jump over the first face
    {
      if (m_num_face==0 || it->finite())
      {
        ++n;
        if (m_cur_she.halfedge()==nullptr && (m_num_face==0 || m_num_face==n))
        { m_cur_she.set(it->strip_id(), it->first_halfedge()); }
        if (m_num_face==0 || m_num_face==n)
        {
          assert(it->first_halfedge()!=nullptr);
          Global_halfedge itf(it->strip_id(), it->first_halfedge());
          Global_halfedge itfend=itf;

          assert(!m_shds.partial_hds(itf).is_external(itf.halfedge()));

          if (it->finite())
          { compute_face(itf); }

          do
          {
            if (!m_shds.is_marked(itf, markvertices))
            {
              compute_vertex(itf);
              Global_halfedge itv=itf;
              do
              {
                m_shds.mark(itv, markvertices);
                itv.set_halfedge(m_shds.hds(itv).next_around_vertex(itv.halfedge()));
              }
              while(itv!=itf);
            }

            if (!m_shds.is_marked(itf, markedges))
            {
              compute_edge(itf);
              m_shds.mark(itf, markedges);
              m_shds.mark(m_shds.opposite(itf), markedges);
            }

            m_shds.move_to_next(itf);
          }
          while(itf!=itfend);
        }
      }
    }

    for (std::size_t i=0; i<m_shds.number_of_strips(); ++i)
    {
      for (auto it=m_shds.hds(i).halfedges().begin(),
           itend=m_shds.hds(i).halfedges().end(); it!=itend; ++it)
      {
        m_shds.unmark(it, markvertices);
        m_shds.unmark(it, markedges);
      }
    }

    m_shds.free_mark(markedges);
    m_shds.free_mark(markvertices);
  }

  virtual void init()
  {
    Base::init();

    setKeyDescription(::Qt::Key_B, "Toggles display grid borders");

    setKeyDescription(::Qt::Key_D, "Move current halfedge by next");
    setKeyDescription(::Qt::ControlModifier+::Qt::Key_D, "Move current halfedge by previous");
    setKeyDescription(::Qt::ShiftModifier+::Qt::Key_D, "Move current halfedge by opposite");

    setKeyDescription(::Qt::Key_Z+::Qt::ControlModifier, "Draw all faces");
    setKeyDescription(::Qt::ControlModifier+::Qt::Key_Left, "Decrement current face drawn");
    setKeyDescription(::Qt::ControlModifier+::Qt::Key_Right, "Increment current face drawn");

    setKeyDescription(::Qt::Key_T, "Increment current strip drawn");
    setKeyDescription(::Qt::ShiftModifier+::Qt::Key_T, "Decrement current strip drawn");
    setKeyDescription(::Qt::Key_T+::Qt::ControlModifier, "Draw all strips");

    setMouseBindingDescription(::Qt::ControlModifier, ::Qt::LeftButton, "Display position of the point");
  }

  virtual void keyPressEvent(QKeyEvent *e)
  {
    const ::Qt::KeyboardModifiers modifiers = e->modifiers();
    if ((e->key()==::Qt::Key_B) && (modifiers==::Qt::NoButton))
    {
      m_draw_grid=!m_draw_grid;
      displayMessage(QString("Draw grid=%1.").arg(m_draw_grid?"true":"false"));
      compute_elements();
      redraw();
    }
    else if ((e->key()==::Qt::Key_D) && (modifiers==::Qt::NoButton))
    {
      m_shds.move_to_next(m_cur_she);
      displayMessage(QString("Cur halfedge move by next."));
      compute_elements();
      redraw();
    }
    else if ((e->key()==::Qt::Key_D) && (modifiers==::Qt::ControlModifier))
    {
      m_shds.move_to_previous(m_cur_she);
      displayMessage(QString("Cur halfedge move by previous."));
      compute_elements();
      redraw();
    }
    else if ((e->key()==::Qt::Key_D) && (modifiers==::Qt::ShiftModifier))
    {
      m_shds.move_to_opposite(m_cur_she);
      displayMessage(QString("Cur halfedge move by opposite."));
      compute_elements();
      redraw();
    }
    else if ((e->key()==::Qt::Key_Z) && (modifiers==::Qt::ControlModifier))
    {
      m_num_face=0;
      if (m_cur_she.halfedge()==nullptr && !m_shds.faces().empty())
      {
        m_cur_she.set(0, m_shds.hds(0).halfedges().begin());
        m_cur_she.set(m_shds.faces().begin()->strip_id(),
                       m_shds.faces().begin()->first_halfedge());
      }
      displayMessage(QString("Draw all faces."));
      compute_elements();
      redraw();
    }
    else if ((e->key()==::Qt::Key_Right) && (modifiers==::Qt::ControlModifier))
    {
      ++m_num_face; m_cur_she.set_halfedge(nullptr);
      if (m_num_face==m_shds.number_of_finite_faces()+1)
      {
        m_num_face=0;
        displayMessage(QString("Draw all faces."));
      }
      else
      {
        displayMessage(QString("Draw face num %1.").arg(m_num_face));
      }
      compute_elements();
      redraw();
    }
    else if ((e->key()==::Qt::Key_Left) && (modifiers==::Qt::ControlModifier))
    {
      m_cur_she.set_halfedge(nullptr);
      if (m_num_face==0)
      { m_num_face=m_shds.number_of_finite_faces(); }
      else { --m_num_face; }

      if (m_num_face==0)
      { displayMessage(QString("Draw all faces.")); }
      else
      { displayMessage(QString("Draw face num %1.").arg(m_num_face)); }
      compute_elements();
      redraw();
    }
    else if ((e->key()==::Qt::Key_T) && (modifiers==::Qt::NoButton))
    {
      ++m_cur_strip;
      if (m_cur_strip==m_shds.number_of_strips()+1)
      {
        m_cur_strip=0;
        displayMessage(QString("Draw all strips."));
      }
      else
      { displayMessage(QString("Draw strip num %1.").arg(m_cur_strip-1)); }
      compute_elements();
      redraw();
    }
    else if ((e->key()==::Qt::Key_T) && (modifiers==::Qt::ShiftModifier))
    {
      if (m_cur_strip==0)
      { m_cur_strip=m_shds.number_of_strips(); }
      else { --m_cur_strip; }

      if (m_cur_strip==0)
      { displayMessage(QString("Draw all strips.")); }
      else
      { displayMessage(QString("Draw strip num %1.").arg(m_cur_strip-1)); }
      compute_elements();
      redraw();
    }
    else if ((e->key()==::Qt::Key_T) && (modifiers==::Qt::ControlModifier))
    {
      m_cur_strip=0;
      displayMessage(QString("Draw all strips."));
      compute_elements();
      redraw();
    }
    else
    { Base::keyPressEvent(e); } // Call the base method to process others/classicals key

    // Call: * compute_elements() if the model changed, followed by
    //       * redraw() if some viewing parameters changed that implies some
    //                  modifications of the buffers
    //                  (eg. type of normal, color/mono)
    //       * update() just to update the drawing
  }

  void mousePressEvent (QMouseEvent * m)
  {
    if (m->modifiers()==Qt::ControlModifier)
    {
      CGAL::qglviewer::Vec globalp=camera()->unprojectedCoordinatesOf
                                   (CGAL::qglviewer::Vec(m->x(), m->y(), 0));
      displayMessage(QString("Point (%1, %2).").arg(globalp.x).arg(globalp.y));
    }
    Base::mousePressEvent(m);
  }

protected:
  SHds& m_shds;
  bool m_nofaces;
  bool m_random_face_color;
  std::size_t m_num_face;
  Global_halfedge m_cur_she;
  bool m_draw_grid;
  std::size_t m_cur_strip;

  CGAL::Color m_color_points;
  CGAL::Color m_color_grid;
  CGAL::Color m_color_selected_he;
  CGAL::Color m_color_above_links;
};

void draw_shds(SHds& ashds,
               const char* title="Basic Viewer for sHDS",
               bool nofill=false)
{
#if defined(CGAL_TEST_SUITE)
  bool cgal_test_suite=true;
#else
  bool cgal_test_suite=qEnvironmentVariableIsSet("CGAL_TEST_SUITE");
#endif

  if (!cgal_test_suite)
  {
    int argc=1;
    const char* argv[2]={"hdsviewer","\0"};
    QApplication app(argc,const_cast<char**>(argv));
    MySimpleSHDSViewerQt mainwindow(app.activeWindow(), ashds, title, nofill);
    mainwindow.show();
    app.exec();
  }
}

#else // CGAL_USE_BASIC_VIEWER

void draw_shds(SHds& /*ashds*/,
               const char* /*title*/="Basic Viewer for sHDS",
               bool /*nofill*/=false)
{
}


#endif // CGAL_USE_BASIC_VIEWER

#endif // DRAW_SHDS_H
