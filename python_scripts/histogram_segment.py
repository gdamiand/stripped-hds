import argparse
import numpy as np
import matplotlib.collections as mcollec
import matplotlib.colors as mcolors
import matplotlib.pyplot as plt
import os

def make_size_bar(ax, size, bins):

    ax.hist(size, bins=bins, density=True)

    ax.set_xlabel("size")
    ax.set_ylabel("frequency")
    ax.grid(True)

    return ax

def make_dir_bar(ax, dir, bins):

    ax.hist(dir, bins=bins, density=True)

    ax.set_xlabel("direction [deg]")
    ax.set_ylabel("frequency")
    ax.grid(True)

    return ax

def make_pos_bar(ax, x, y, bins):

    hist = ax.hist2d(x, y, bins=bins, density=True)
    cbar = plt.colorbar(hist[3], ax=ax)

    ax.set_xlabel("x")
    ax.set_ylabel("y")
    cbar.ax.set_ylabel("frequency")

    return ax

if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument("input",
                        help="input result file")
    parser.add_argument("--bins", "-b", type=int, default=16,
                        help="set histogram number of bins")

    parser.add_argument("--split", "-s", action="store_true",
                        help="one figure per graph instead of subplots")
    parser.add_argument("--draw", "-d", action="store",
                        help="draw segments on another figure and export to PDF")
    parser.add_argument("--export", "-e", action="store_true",
                        help="export histograms")
    parser.add_argument("--noshow", "-n", action="store_true",
                        help="dont show figures")
    parser.add_argument("--ext", "-x", action="store", default="png",
                        help="set export extension, default is png")

    args = parser.parse_args()

    data = np.genfromtxt(args.input, dtype=float, skip_header=1)

    x0 = data[:, 0]
    y0 = data[:, 1]
    x1 = data[:, 2]
    y1 = data[:, 3]

    dx = x1 - x0
    dy = y1 - y0

    size = np.sqrt(np.square(dx) + np.square(dy))
    dir = np.arctan(dy / dx) * 180.0 / np.pi

    x = 0.5 * (x0 + x1)
    y = 0.5 * (y0 + y1)

    basename = os.path.splitext(os.path.basename(args.input))[0]

    if args.split:

        fig0 = plt.figure()
        fig1 = plt.figure()
        fig2 = plt.figure()

        make_size_bar(fig0.add_subplot(), size, args.bins)
        make_dir_bar (fig1.add_subplot(), dir,  args.bins)
        make_pos_bar (fig2.add_subplot(), x, y, args.bins)

        if args.export:
            fig0.savefig("%s-hist-size.%s" % (basename, args.ext))
            fig1.savefig("%s-hist-dir.%s"  % (basename, args.ext))
            fig2.savefig("%s-hist-pos.%s"  % (basename, args.ext))

    else:

        fig = plt.figure()

        make_size_bar(fig.add_subplot(1, 3, 1), size, args.bins)
        make_dir_bar (fig.add_subplot(1, 3, 2), dir,  args.bins)
        make_pos_bar (fig.add_subplot(1, 3, 3), x, y, args.bins)

        if args.export:
            fig.savefig("%s-hist.%s" % (basename, args.ext))

    if args.draw:

        palette = list(mcolors.TABLEAU_COLORS.values())
        n = len(palette)

        lines = []
        colors = []
        for i in range(data.shape[0]):

            row = data[i, :]
            lines.append([(row[0], row[1]), (row[2], row[3])])

            colors.append(palette[i % n])

        fig = plt.figure()
        ax  = fig.add_axes([0, 0, 1, 1])

        ax.add_collection(mcollec.LineCollection(lines, color=colors))
        ax.autoscale()
        ax.axis('off')

        fig.savefig(args.draw)

    if not args.noshow:
        plt.show()
