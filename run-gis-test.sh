#!/bin/bash

################################################################################
# Run tests on all the gis files
# Usage run-gis-test [REPEAT]
#     REPEAT is the number of runs to do for each file; computation time will be
#            the average of these runs. 1 by default.
REPEAT=1 # Number of runs; computation time will be the average
PROG="./builds/release/parallel-arrangement-bench" # Executable name

if (( $# > 0 ))
then
   REPEAT=${1}
fi   

# Test function: 1st parameter: directory that contains segment files;  2nd parameter: options
function test {
echo "######################################## ${REPEAT} ${2} ###############################" >> times.dat
echo "######################################## ${REPEAT} ${2} ################################" >> detailed-times.dat
echo "######################################## ${REPEAT} ${2} ###############################" >> nbcells.dat

for file in "${1}/"*.txt
do
    ${PROG} -t1 "${file}" -cgal -repeat ${REPEAT}
    for i in 1 2 4 8 16 32
    do
        ${PROG} -t1 "${file}" -nbs $((i)) -nbt $((i)) -repeat ${REPEAT} ${2}
    done
    echo "" >> times.dat
    echo "" >> detailed-times.dat
done
}

echo "################################################################################" >> times.dat
echo "################################################################################" >> detailed-times.dat
echo "################################################################################" >> nbcells.dat

echo "#GlobalTimes CGAL 1 2 4 8 16 32" >> times.dat
echo "#DetailedTimes LoadAndDispatch ComputeLocals ComputeFaces" >> detailed-times.dat
echo "#NbCells HalfEdges Vertices Edges FiniteFaces Faces ExternalHalfedges" >> nbcells.dat

test ./data/countries "-crop"
