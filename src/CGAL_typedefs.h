// compute_arrangement: a new method to compute arrangement of segments.
// Copyright (C) 2020 CNRS and LIRIS' Establishments (France).
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
// Author(s)     : Guillaume Damiand <guillaume.damiand@liris.cnrs.fr>
//

#ifndef CGAL_TYPEDEFS_H
#define CGAL_TYPEDEFS_H

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <CGAL/Linear_cell_complex_for_combinatorial_map.h>
#include <CGAL/Compact_container.h>
#include <CGAL/Real_timer.h>
#include <CGAL/Timer.h>
#include <vector>

template<typename LCC>
class Inner_point;

///////////////////////////////////////////////////////////////////////////////
typedef CGAL::Exact_predicates_inexact_constructions_kernel EPICK;
typedef CGAL::Exact_predicates_exact_constructions_kernel   EPECK;
typedef EPECK::FT              ECoord;
typedef EPECK::Point_2         EPoint;
typedef EPECK::Segment_2       ESegment;
typedef EPECK::Iso_rectangle_2 ERectangle;
typedef EPICK::FT              ICoord;
typedef EPICK::Point_2         IPoint;
typedef EPICK::Segment_2       ISegment;
typedef EPICK::Iso_rectangle_2 IRectangle;
///////////////////////////////////////////////////////////////////////////////
// typedef CGAL::Timer My_timer;
typedef CGAL::Real_timer My_timer;
///////////////////////////////////////////////////////////////////////////////
template<typename Ref>
struct Info_for_face
{
  using Face_attribute_handle=
         typename Ref::template Attribute_handle<2>::type;

  Info_for_face() : m_finite(true), m_father(nullptr)
  {}

  bool m_finite;
  std::vector<Face_attribute_handle> m_son;
  Face_attribute_handle m_father;
};
////////////////////////////////////////////////////////////////////////////////
template<typename Ref>
struct Info_for_vertex
{
  using Dart_handle=typename Ref::Dart_handle;

  Info_for_vertex(Dart_handle dh=nullptr) : m_dart_above(dh)
  {}

  Dart_handle m_dart_above;
};
////////////////////////////////////////////////////////////////////////////////
struct Myitem
{
  template<class Ref>
  struct Dart_wrapper
  {
    typedef CGAL::Cell_attribute_with_point<Ref, Info_for_vertex<Ref>> Vertex_attribute;
    typedef CGAL::Cell_attribute<Ref, Info_for_face<Ref>> Face_attribute;
    typedef std::tuple<Vertex_attribute,void,Face_attribute> Attributes;
  };
};
typedef CGAL::Linear_cell_complex_traits
<2, CGAL::Exact_predicates_exact_constructions_kernel> Traits;
 typedef CGAL::Linear_cell_complex_for_combinatorial_map
       <2,2,Traits,Myitem,CGAL_ALLOCATOR(int),
       CGAL::Combinatorial_map_base,
       CGAL::CMap_linear_cell_complex_storage_1
            <2, 2, Traits, Myitem, CGAL_ALLOCATOR(int), CGAL::Tag_true>
> LCC_2;
///////////////////////////////////////////////////////////////////////////////
typedef LCC_2::Dart_handle Dart_handle;
typedef LCC_2::Dart_const_handle Dart_const_handle;
///////////////////////////////////////////////////////////////////////////////

#endif // CGAL_TYPEDEFS_H
