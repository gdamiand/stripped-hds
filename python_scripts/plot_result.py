import argparse
import itertools
import numpy as np
import matplotlib.colors as mcolors
import matplotlib.pyplot as plt
import os
import warnings

# PB: see "matplotlib named_colors"
PREFERED_COLORS = {
    "load":   mcolors.TABLEAU_COLORS["tab:blue"],
    "local":  mcolors.TABLEAU_COLORS["tab:orange"],
    "global": mcolors.TABLEAU_COLORS["tab:green"],
    "idle":   mcolors.TABLEAU_COLORS["tab:red"],
}

def last_word(line):
    return line.rsplit(" ", 1)[1]

def parse_total(lines,
                cgal=False):
    index = 4 if cgal else 10
    return float(last_word(lines[index]))

def parse_load(lines,
               cgal=False):
    index = 3 if cgal else 7
    return float(last_word(lines[index]))

def parse_local(lines):
    return float(last_word(lines[8]))

def parse_global(lines):
    return float(last_word(lines[9]))

def parse_thread(lines):

    kv = {}
    for line in lines[1].split("[")[1:]: # data per thread
        line = line.split()
        b = int(line[2].split("=")[1])
        t = float(line[-1].split("=")[1])
        kv[b] = t

    return [ kv[k] for k in sorted(kv) ]

def parse_memory(lines,
                 cgal=False):
    index = 2 if cgal else 6
    return int(lines[index].split(" ", 3)[2])

def parse_traverse(lines):
    lines = lines[11].split(" times ", 1)
    n = int(lines[0].rsplit(" ", 1)[1])
    t = float(lines[1].split(" ", 1)[0])
    return t / n

def parse_cdarts(lines):

    total = 0
    for line in lines[3].split("|")[0:-1]: # data per band
        line = line.rsplit(", ", 1)
        lcd = int(line[0].rsplit("(", 1)[1])
        rcd = int(line[1].rsplit(")", 1)[0])
        total += lcd + rcd

    return total

def read_log(filename):

    data = {}

    with open(filename) as file:

        it = iter(file)
        for _ in it:

            line = next(it).strip()
            method = line.rsplit(" ", 1)[1]

            if method == "cgal":

                if "cgal" not in data:
                    data["cgal"] = {
                        "total":  { "detail": [] },
                        "load":   { "detail": [] },
                        "global": { "detail": [] },
                        "memory": { "detail": [] }
                    }

                entry = data["cgal"]
                lines = [ next(it).strip() for _ in range(5) ]

                total = parse_total (lines, cgal=True)
                load  = parse_load  (lines, cgal=True)
                entry["total"]["detail"] .append(total)
                entry["load"] ["detail"] .append(load)
                entry["global"]["detail"].append(total - load)
                entry["memory"]["detail"].append(parse_memory(lines, cgal=True))

            else:

                nbthread = int(method)

                if method not in data:
                    data[method] = {
                        "total":    { "detail": [] },
                        "load" :    { "detail": [] },
                        "local":    { "detail": [] },
                        "global":   { "detail": [] },
                        "thread":   { "detail": [ [] for _ in range(nbthread) ] },
                        "memory":   { "detail": [] },
                        "traverse": { "detail": [] },
                        "cdarts":   { "detail": [] }
                    }

                entry = data[method]
                lines = [ next(it).strip() for _ in range(12) ]

                entry["total"]   ["detail"].append(parse_total (lines))
                entry["load"]    ["detail"].append(parse_load  (lines))
                entry["local"]   ["detail"].append(parse_local (lines))
                entry["global"]  ["detail"].append(parse_global(lines))
                entry["memory"]  ["detail"].append(parse_memory(lines))
                entry["traverse"]["detail"].append(parse_traverse(lines))
                entry["cdarts"]  ["detail"].append(parse_cdarts(lines))

                try :
                    thread = parse_thread(lines)
                    for i in range(nbthread):
                        entry["thread"]["detail"][i].append(thread[i])
                except:
                    warnings.warn("found thread without band in")
                    for i in range(nbthread):
                        entry["thread"]["detail"][i].append(np.nan)

    for method in data:
        for key in data[method]:

            entry = data[method][key]

            arr = np.array(entry["detail"]).transpose()
            entry["mean"] = np.nanmean(arr, axis=0)
            entry["dev"]  = np.nanstd(arr, axis=0)

    return data

def remove_idle(data):

    for m in data:
        if m != "cgal":

            t_local  = data[m]["local"] ["mean"]
            t_thread = data[m]["thread"]["mean"]
            offset = np.min(t_local - t_thread)

            data[m]["local"]["mean"] -= offset
            data[m]["total"]["mean"] -= offset

    return data

#def fill_cgal(data):
#
#    if "cgal" in data:
#
#        entry = data["cgal"]
#
#        entry["load"]     = { "mean": 0.0, "dev": 0.0 }
#        entry["local"]    = { "mean": 0.0, "dev": 0.0 }
#        entry["global"]   = entry["total"]
#        entry["thread"]   = { "mean": np.array([ 0.0 ]), "dev": np.array([ 0.0 ]) }
#        entry["memory"]   = { "mean": 0.0, "dev": 0.0 }
#        entry["traverse"] = { "mean": 0.0, "dev": 0.0 }
#
#    return data
#
#def extract_field(data, methods, field):
#    mean = list(map(lambda m: data[m][field]["mean"], methods))
#    dev  = list(map(lambda m: data[m][field]["dev"],  methods))
#    return mean, dev

def extract_field(data, methods, field):
    mean = []
    dev  = []
    for m in methods:
        if field in data[m]:
            mean.append(data[m][field]["mean"])
            dev .append(data[m][field]["dev"])
        else:
            if field == "thread":
                mean.append([ 0.0 ])
                dev .append([ 0.0 ])
            else:
                mean.append(0.0)
                dev .append(0.0)
    return mean, dev

def make_simple_bar(ax, data, methods, field, without_error):

    mean, dev = extract_field(data, methods, field)
    ind = np.arange(0, len(methods))

    b = ax.bar(ind, mean, yerr=dev)

    if without_error:
        for e in b.errorbar[2]:
            e.remove()

    ax.set_xticks(ind)
    ax.set_xticklabels(methods)
    ax.grid(True)

    return ax

def make_total_bar(ax, data,
                   without_error=False, without_load=False):

    methods   = data.keys()
    mean, dev = extract_field(data, methods, "total")

    ind = np.arange(0, len(methods))

    if without_load:
        load_mean, _ = extract_field(data, methods, "load")
        y = np.array(mean) - np.array(load_mean)
    else:
        y = mean
    b = ax.bar(ind, y, yerr=dev)

    if without_error:
        for e in b.errorbar[2]:
            e.remove()

    ax.set_xticks(ind)
    ax.set_xticklabels(methods)
    ax.set_xlabel("method - threads")
    ax.set_ylabel("duration [sec]")
    ax.grid(True)

    return ax

def make_step_bar(ax, data,
                  without_error=False, without_load=False):

    methods         = data.keys()
    t0_mean, t0_dev = extract_field(data, methods, "load")
    t1_mean, t1_dev = extract_field(data, methods, "local")
    t2_mean, t2_dev = extract_field(data, methods, "global")

    ind = np.arange(0, len(methods))

    if without_load:

        p1 = ax.bar(ind, t1_mean, yerr=t1_dev,                 color=PREFERED_COLORS["local"])
        p2 = ax.bar(ind, t2_mean, yerr=t2_dev, bottom=t1_mean, color=PREFERED_COLORS["global"])

        bars  = (p1, p2)
        steps = ("local", "global")

    else:

        p0 = ax.bar(ind, t0_mean, yerr=t0_dev,                                                 color=PREFERED_COLORS["load"])
        p1 = ax.bar(ind, t1_mean, yerr=t1_dev, bottom=t0_mean,                                 color=PREFERED_COLORS["local"])
        p2 = ax.bar(ind, t2_mean, yerr=t2_dev, bottom=(np.array(t0_mean) + np.array(t1_mean)), color=PREFERED_COLORS["global"])

        bars  = (p0, p1, p2)
        steps = ("load", "local", "global")

    if without_error:
        for e in itertools.chain(*[ b.errorbar[2] for b in bars ]):
            e.remove()

    ax.set_xticks(ind)
    ax.set_xticklabels(methods)
    ax.set_xlabel("method - threads")
    ax.set_ylabel("duration [sec]")
    ax.grid(True)

    ax.legend([ b[0] for b in bars ], steps)

    return ax

def make_detail_bar(ax, data,
                    without_error=False, without_load=False):

    methods         = list(data.keys())
    t0_mean, t0_dev = extract_field(data, methods, "load")
    t1_mean, _      = extract_field(data, methods, "local")
    t2_mean, t2_dev = extract_field(data, methods, "global")
    t3_mean, t3_dev = extract_field(data, methods, "thread")

    ind = np.arange(0, len(methods))

    w = 0.8
    x      = []
    height = []
    width  = []
    yerr   = []
    bottom = []

    height_idle = []
    bottom_idle = []

    for i in range(len(methods)):

        n = 1 if methods[i] == "cgal" else int(methods[i])
        wi = w / float(n)
        a = (w - wi) / (float(n) - 1.0) if n != 1 else 0.0
        b = float(ind[i]) + 0.5 * (wi - w)

        for j in range(n):

            t0_mean_i = 0.0 if without_load else t0_mean[i]

            x     .append(a * j + b)
            height.append(t3_mean[i][j])
            width .append(wi)
            yerr  .append(t3_dev[i][j])
            bottom.append(t0_mean_i)

            height_idle.append(t1_mean[i] - t3_mean[i][j])
            bottom_idle.append(t0_mean_i  + t3_mean[i][j])

    if without_load:

        p1 = ax.bar(x,   height,  yerr=yerr,   bottom=bottom, width=width, color=PREFERED_COLORS["local"])
        p2 = ax.bar(ind, t2_mean, yerr=t2_dev, bottom=t1_mean,             color=PREFERED_COLORS["global"])

        p_idle = ax.bar(x, height_idle, bottom=bottom_idle, width=width, color=PREFERED_COLORS["idle"])

        bars  = (p1, p2, p_idle)
        steps = ("local", "global", "idle")

        bars_with_error = (p1, p2)

    else:

        p0 = ax.bar(ind, t0_mean, yerr=t0_dev,                                                 color=PREFERED_COLORS["load"])
        p1 = ax.bar(x,   height,  yerr=yerr,   bottom=bottom, width=width,                     color=PREFERED_COLORS["local"])
        p2 = ax.bar(ind, t2_mean, yerr=t2_dev, bottom=(np.array(t0_mean) + np.array(t1_mean)), color=PREFERED_COLORS["global"])

        p_idle = ax.bar(x, height_idle, bottom=bottom_idle, width=width, color=PREFERED_COLORS["idle"])

        bars  = (p0, p1, p2, p_idle)
        steps = ("load", "local", "global", "idle")

        bars_with_error = (p0, p1, p2)

    if without_error:
        for e in itertools.chain(*[ b.errorbar[2] for b in bars_with_error ]):
            e.remove()
    else:
        # PB: hide thread error
        for e in p1.errorbar[2]:
            e.remove()

    ax.set_xticks(ind)
    ax.set_xticklabels(methods)
    ax.set_xlabel("method - threads")
    ax.set_ylabel("duration [sec]")
    ax.grid(True)

    ax.legend([ b[0] for b in bars ], steps)

    return ax

def make_memory_bar(ax, data,
                    without_error=False):

    ax = make_simple_bar(ax, data, data.keys(), "memory", without_error)
    ax.set_xlabel("threads")
    ax.set_ylabel("memory [bytes]")

    return ax

def make_traverse_bar(ax, data,
                      without_error=False):

    methods   = list(filter(lambda m: m != "cgal", data.keys()))
    ax = make_simple_bar(ax, data, methods, "traverse", without_error)
    ax.set_xlabel("threads")
    ax.set_ylabel("traversal duration [sec]")

    return ax

def print_data(data, *args,
               spacing=20):

    unsupported = [ "thread" ]

    methods    = data.keys()
    row_format = ("{:>" + str(spacing) + "}") * (len(methods) + 1)
    print(row_format.format("", *methods))

    for field in args:

        if field in unsupported:
            warnings.warn("'print_data' does not support '" + field + "' field")
            continue

        mean, _ = extract_field(data, methods, field)
        print(row_format.format(field, *mean))

if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument("input", nargs="*",
                        help="input result files")

    parser.add_argument("--split", "-s", action="store_true",
                        help="one figure per graph instead of subplots")
    parser.add_argument("--export", "-e", action="store_true",
                        help="export plots")
    parser.add_argument("--ext", "-x", action="store", default="png",
                        help="set export extension, default is png")

    parser.add_argument("--no-error", action="store_true",
                        help="hide error bar")
    parser.add_argument("--no-load", action="store_true",
                        help="hide load bar")

    parser.add_argument("--hide", action="store_true",
                        help="disable show")

    args = parser.parse_args()

    for input in args.input:

        data = read_log(input)
        # PB: hack to remove idle
        data = remove_idle(data)

        basename = os.path.splitext(os.path.basename(input))[0]

        # if args.split:
        if True:
            #fig0 = plt.figure()
            #fig1 = plt.figure()
            fig2 = plt.figure()
            #fig3 = plt.figure()
            #fig4 = plt.figure()

            #fig0.canvas.set_window_title("%s - total"  % input)
            #fig1.canvas.set_window_title("%s - step"   % input)
            fig2.canvas.set_window_title("%s - detail" % input)
            #fig3.canvas.set_window_title("%s - memory" % input)
            #fig4.canvas.set_window_title("%s - traverse" % input)

            #ax0  = fig0.add_subplot()
            #ax1  = fig1.add_subplot(sharex=ax0, sharey=ax0)
            ax2  = fig2.add_subplot() #sharex=ax0, sharey=ax0)
            #ax3  = fig3.add_subplot()
            #ax4  = fig4.add_subplot()

            #make_total_bar   (ax0, data, without_error=args.no_error, without_load=args.no_load)
            #make_step_bar    (ax1, data, without_error=args.no_error, without_load=args.no_load)
            make_detail_bar  (ax2, data, without_error=args.no_error, without_load=args.no_load)
            #make_memory_bar  (ax3, data, without_error=args.no_error)
            #make_traverse_bar(ax4, data, without_error=args.no_error)

            if args.export:
                #fig0.savefig("%s-total.%s"    % (basename, args.ext))
                #fig1.savefig("%s-step.%s"     % (basename, args.ext))
                fig2.savefig("%s-detail.%s"   % (basename, args.ext))
                #fig3.savefig("%s-memory.%s"   % (basename, args.ext))
                #fig4.savefig("%s-traverse.%s" % (basename, args.ext))

        # else:

            # fig = plt.figure()
            # fig.canvas.set_window_title(input)

            # ax0  = fig.add_subplot(1, 5, 1)
            # ax1  = fig.add_subplot(1, 5, 2, sharex=ax0, sharey=ax0)
            # ax2  = fig.add_subplot(1, 5, 3, sharex=ax0, sharey=ax0)
            # ax3  = fig.add_subplot(1, 5, 4)
            # ax4  = fig.add_subplot(1, 5, 5)

            # make_total_bar   (ax0, data, without_error=args.no_error, without_load=args.no_load)
            # make_step_bar    (ax1, data, without_error=args.no_error, without_load=args.no_load)
            # make_detail_bar  (ax2, data, without_error=args.no_error, without_load=args.no_load)
            # make_memory_bar  (ax3, data, without_error=args.no_error)
            # make_traverse_bar(ax4, data, without_error=args.no_error)

            # if args.export:
            #     fig.savefig("%s.%s" % (basename, args.ext))
            
        print_data(data, "memory", "traverse", "cdarts")

    if not args.hide:
        plt.show()
