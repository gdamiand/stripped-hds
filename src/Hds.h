// compute_arrangement: a new method to compute arrangement of segments.
// Copyright (C) 2020 CNRS and LIRIS' Establishments (France).
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
// Author(s)     : Guillaume Damiand <guillaume.damiand@liris.cnrs.fr>
//

#ifndef HDS_H
#define HDS_H

#include <bitset>

#include "CGAL_typedefs.h"
#include "Print_txt.h"
#include "Export_xfig.hh"

///////////////////////////////////////////////////////////////////////////////
class Halfedge;
class Vertex;
class Face;
class HDS;
class SHds;
////////////////////////////////////////////////////////////////////////////////
typedef CGAL::Compact_container<Halfedge>::iterator Halfedge_handle;
typedef CGAL::Compact_container<Halfedge>::const_iterator Halfedge_const_handle;
typedef CGAL::Compact_container<Vertex>::iterator Vertex_handle;
typedef CGAL::Compact_container<Face>::iterator Face_handle;
////////////////////////////////////////////////////////////////////////////////
class Halfedge
{
public:
  friend class HDS;
  friend class Vertex;
  friend class SHds;

  typedef uint32_t size_type;
  static const size_type NB_MARKS = 32;

  Halfedge(Halfedge_handle opposite=nullptr,
           Vertex_handle vertex=nullptr,
           int edge_id=0) :
    m_opposite(opposite),
    m_prev_around_vertex(nullptr),
    m_next_around_vertex(nullptr),
    m_vertex(vertex),
    m_face(nullptr),
    m_edge_id(edge_id)
  {}

  void set( Halfedge_handle opposite,
            Halfedge_handle prev_around_vertex,
            Halfedge_handle next_around_vertex,
            Vertex_handle   vertex,
            int             edge_id)
  {
    m_opposite=opposite;
    m_prev_around_vertex=prev_around_vertex;
    m_next_around_vertex=next_around_vertex;
    m_vertex=vertex;
    m_edge_id=edge_id;
  }

  Halfedge_handle opposite() const
  { return m_opposite; }
  Halfedge_handle prev_around_vertex() const
  { return m_prev_around_vertex; }
  Halfedge_handle next_around_vertex() const
  { return m_next_around_vertex; }
  Vertex_handle vertex() const
  { return m_vertex; }
  Face_handle face() const
  { return m_face; }
 int edge_id() const
  { return m_edge_id; }

 bool get_mark(size_type amark) const
 {
   CGAL_assertion(amark>=0 && amark<NB_MARKS);
   return m_marks[amark];
 }
 void set_mark(size_type amark, bool avalue) const
 {
   CGAL_assertion(amark>=0 && amark<NB_MARKS);
   m_marks.set(amark, avalue);
 }
 void flip_mark(size_type amark) const
 {
   CGAL_assertion(amark>=0 && amark<NB_MARKS);
   m_marks.flip(amark);
 }
 std::bitset<NB_MARKS> get_marks() const
 { return m_marks; }
 void set_marks(const std::bitset<NB_MARKS>& amarks) const
 { m_marks=amarks; }

  void* for_compact_container() const
  { return m_opposite.for_compact_container(); }
  void for_compact_container(void* p)
  { m_opposite.for_compact_container(p);}

protected:
  Halfedge_handle m_opposite;
  Halfedge_handle m_prev_around_vertex;
  Halfedge_handle m_next_around_vertex;
  Vertex_handle   m_vertex;
  Face_handle     m_face;
  int             m_edge_id; // >0 if halfedge from left to right or vertical and bottom to top, <0 otherwise
                             // two halfedges of the same edge have the same value and opposite sign
  mutable std::bitset<NB_MARKS> m_marks;
};
////////////////////////////////////////////////////////////////////////////////
class Global_halfedge
{
public:
  friend class SHds;

  Global_halfedge(std::size_t strip_id=0,
                  Halfedge_handle hh=nullptr):
    m_strip_id(strip_id),
    m_hh(hh)
  {}

  void set(std::size_t strip_id=0, Halfedge_handle hh=nullptr)
  { m_strip_id=strip_id; m_hh=hh; }

  void set_halfedge(Halfedge_handle hh)
  { m_hh=hh; }

  std::size_t strip_id() const
  { return m_strip_id; }

  const Halfedge_handle& halfedge() const
  { return m_hh; }
  Halfedge_handle& halfedge()
  { return m_hh; }

  void go_to_left_strip()
  { assert(m_strip_id>0); --m_strip_id; }
  void go_to_right_strip()
  { ++m_strip_id; }

  bool operator==(const Global_halfedge& other) const
  { return m_hh==other.m_hh; } // Handle are unique, thus we don't need to compare m_strip_id
  bool operator!=(const Global_halfedge& other) const
  { return !operator==(other); }

  bool operator<(const Global_halfedge& other) const
  { return m_strip_id<other.m_strip_id ||
        (m_strip_id==other.m_strip_id && m_hh<other.m_hh); }
  bool operator>(const Global_halfedge& other) const
  { return other.operator<(*this); }
  bool operator>=(const Global_halfedge& other) const
  { return !operator<(other); }
  bool operator<=(const Global_halfedge& other) const
  { return !operator>(other); }

protected:
  std::size_t m_strip_id;
  Halfedge_handle m_hh;
};
////////////////////////////////////////////////////////////////////////////////
class Vertex
{
public:
  friend class HDS;
  friend class Halfedge;

  Vertex() :
    m_first_halfedge(nullptr),
    m_above_halfedge(nullptr)
  {}

  Vertex(const EPoint& p) :
    m_point(p),
    m_first_halfedge(nullptr),
    m_above_halfedge(nullptr)
  {}

  void set(const EPoint& p, Halfedge_handle first_halfedge,
           Halfedge_handle above_halfedge)
  {
    m_point=p;
    m_first_halfedge=first_halfedge;
    m_above_halfedge=above_halfedge;
  }

  const EPoint& point() const
  { return m_point; }

  Halfedge_handle first_halfedge() const
  { return m_first_halfedge; }

  Halfedge_handle above_halfedge() const
  { return m_above_halfedge; }
  void set_above_halfedge(Halfedge_handle dh)
  { m_above_halfedge=dh; }

  bool no_left_edge() const
  {
    return m_first_halfedge->m_edge_id>0; // If the first halfedge is from left to right
    // or vertical and bottom to up, there is no left edge around the vertex
  }

  void serialize_point(std::ostream& os) const
  {
    CGAL::exact(m_point);
    os<<std::setprecision(17)<<m_point;
  }

  void* for_compact_container() const
  { return m_first_halfedge.for_compact_container(); }
  void for_compact_container(void* p)
  { m_first_halfedge.for_compact_container(p);}

protected:
  EPoint          m_point;
  Halfedge_handle m_first_halfedge;
  Halfedge_handle m_above_halfedge; // Halfedge handle of the edge above this vertex (nullptr if no such edge)
};
////////////////////////////////////////////////////////////////////////////////
class Face
{
public:
  friend class HDS;
  friend class Halfedge;
  friend class SHds;

  Face() :
    m_strip_id(0),
    m_first_halfedge(nullptr),
    m_father(nullptr),
    m_finite(true)
  {}

  void set(std::size_t     strip_id,
           Halfedge_handle first_halfedge,
           Face_handle     father,
           bool            finite)
  {
    m_strip_id=strip_id;
    m_first_halfedge=first_halfedge;
    m_father=father;
    m_finite=finite;
  }

  std::size_t strip_id() const
  { return m_strip_id; }

  Halfedge_handle first_halfedge() const
  { return m_first_halfedge; }

  Face_handle father() const
  { return m_father; }

  bool finite() const
  { return m_finite; }

  const std::vector<Face_handle>& sons() const
  { return m_son; }

  void* for_compact_container() const
  { return m_first_halfedge.for_compact_container(); }
  void for_compact_container(void* p)
  { m_first_halfedge.for_compact_container(p);}

protected:
  std::size_t              m_strip_id; // strip id of the first halfedge
  Halfedge_handle          m_first_halfedge;
  Face_handle              m_father;
  std::vector<Face_handle> m_son;
  bool                     m_finite;
};
////////////////////////////////////////////////////////////////////////////////
class HDS
{
public:
  typedef uint32_t size_type;

  typedef CGAL::Compact_container<Halfedge> Halfedges_container;
  typedef CGAL::Compact_container<Vertex>   Vertices_container;

  void clear()
  {
    m_halfedges.clear();
    m_vertices.clear();
  }

  std::size_t number_of_halfedges() const
  { return m_halfedges.size(); }
  std::size_t number_of_vertices() const
  { return m_vertices.size(); }

  Halfedges_container& halfedges()
  { return m_halfedges; }
  Vertices_container& vertices()
  { return m_vertices; }

  const Halfedges_container& halfedges() const
  { return m_halfedges; }
  const Vertices_container& vertices() const
  { return m_vertices; }

  Halfedge_handle opposite(Halfedge_handle hh) const
  { return hh->m_opposite; }
  Halfedge_handle prev_around_vertex(Halfedge_handle hh) const
  { return hh->m_prev_around_vertex; }
  Halfedge_handle next_around_vertex(Halfedge_handle hh) const
  { return hh->m_next_around_vertex; }
  Vertex_handle vertex(Halfedge_handle hh) const
  { return hh->m_vertex; }
  Face_handle face(Halfedge_handle hh) const
  { return hh->m_face; }
  const EPoint& point(Vertex_handle vh) const
  { return vh->point(); }
  const EPoint& point(Halfedge_handle hh) const
  { return point(vertex(hh)); }
 int edge_id(Halfedge_handle hh) const
  { return hh->m_edge_id; }

  void move_to_opposite(Halfedge_handle& hh) const
  { hh=opposite(hh); }
  void move_to_prev_around_vertex(Halfedge_handle& hh) const
  { hh=prev_around_vertex(hh); }
  void move_to_next_around_vertex(Halfedge_handle& hh) const
  { hh=next_around_vertex(hh); }

  // @return true iff halfedge hh is from left to right
  //   (or vertical and from bottom to top)
  bool left_to_right(Halfedge_handle hh) const
  { return edge_id(hh)>0; }
  // @return true iff halfedge hh is from right to left
  bool right_to_left(Halfedge_handle hh) const
  { return edge_id(hh)<0; }

  Vertex_handle create_vertex(const EPoint& p)
  { return m_vertices.emplace(p); }
  void set_vertex(Halfedge_handle hh, Vertex_handle vh)
  { hh->m_vertex=vh; }
  void set_vertex(Halfedge_handle hh, const EPoint& p)
  { set_vertex(hh, create_vertex(p)); }

  Halfedge_handle create_edge(Vertex_handle vh, int segmentid)
  {
    assert(segmentid>0);

    Halfedge_handle hh1=m_halfedges.emplace(nullptr, vh, segmentid);
    Halfedge_handle hh2=m_halfedges.emplace(hh1, nullptr, -segmentid);
    hh1->m_opposite=hh2;

    return hh1;
  }

  void erase_halfedge(Halfedge_handle hh)
  { m_halfedges.erase(hh); }

  void erase_edge(Halfedge_handle hh)
  {
    erase_halfedge(hh->m_opposite);
    erase_halfedge(hh);
  }

  Halfedge_handle add_segment_around_vertex(Halfedge_handle hh,
                                            Vertex_handle vh,
                                            Halfedge_handle prevhh)
  {
    if (prevhh==nullptr)
    {
      assert(vh->first_halfedge()==nullptr);
      vh->m_first_halfedge=hh;
    }
    else
    {
      assert(vh->first_halfedge()!=nullptr);
      prevhh->m_next_around_vertex=hh;
      hh->m_prev_around_vertex=prevhh;
    }
    return hh;
  }

  void end_of_vertex(Vertex_handle vh, Halfedge_handle prevhh)
  {
    assert(vh->first_halfedge()!=nullptr);
    {
      prevhh->m_next_around_vertex=vh->first_halfedge();
      vh->first_halfedge()->m_prev_around_vertex=prevhh;
    }
  }

  friend std::ostream& operator<<(std::ostream& os,
                                  const HDS& hds)
  {
    os<<hds.halfedges().size()<<" "<<hds.vertices().size()<<std::endl;
    for (auto it=hds.vertices().begin(), itend=hds.vertices().end(); it!=itend; ++it)
    {
      assert(it->first_halfedge()!=nullptr);
      it->serialize_point(os);
      os<<" "<<hds.halfedges().index(it->first_halfedge())
       <<" "<<(it->above_halfedge()==nullptr?'-':'+');
      if (it->above_halfedge()!=nullptr)
      { os<<" "<<hds.halfedges().index(it->above_halfedge()); }
      os<<std::endl;
    }

    os<<std::endl;
    for (auto it=hds.halfedges().begin(), itend=hds.halfedges().end(); it!=itend; ++it)
    {
      assert(it->opposite()!=nullptr);
      assert(it->vertex()==nullptr || it->prev_around_vertex()!=nullptr);
      assert(it->vertex()==nullptr || it->next_around_vertex()!=nullptr);

      os<<" "<<hds.halfedges().index(it->opposite())
       <<" "<<(it->vertex()!=nullptr?'+':'-');
      if (it->vertex()!=nullptr)
      {
        os<<" "<<hds.halfedges().index(it->prev_around_vertex())
         <<" "<<hds.halfedges().index(it->next_around_vertex())
        <<" "<<hds.vertices().index(it->vertex());
      }
      os<<" "<<it->edge_id()<<std::endl;
    }
    return os;
  }

  void load(std::istream& is,
            std::vector<Halfedge_handle>& halfedges,
            std::vector<Vertex_handle>& vertices)
  {
    clear();
    halfedges.clear();
    vertices.clear();

    std::size_t nb_halfedges, nb_vertices;
    is>>nb_halfedges>>nb_vertices;

    halfedges.reserve(nb_halfedges);
    vertices.reserve(nb_vertices);

    for (std::size_t i=0; i<nb_vertices; ++i)
    { vertices.push_back(m_vertices.emplace()); }
    for (std::size_t i=0; i<nb_halfedges; ++i)
    { halfedges.push_back(m_halfedges.emplace()); }

    EPoint p;
    std::size_t ind1, ind2, ind3, ind4;
    int int1;
    char c;

    for (std::size_t i=0; i<nb_vertices; ++i)
    {
      is>>p>>ind1>>c;
      if (c=='+') is>>ind2;
      vertices[i]->set(p, halfedges[ind1], (c=='+'?halfedges[ind2]:nullptr));
    }
    for (std::size_t i=0; i<nb_halfedges; ++i)
    {
      is>>ind1>>c;
      if (c=='+') is>>ind2>>ind3>>ind4;
      is>>int1;
      halfedges[i]->set(halfedges[ind1],
                        (c=='+'?halfedges[ind2]:nullptr),
                        (c=='+'?halfedges[ind3]:nullptr),
                        (c=='+'?vertices[ind4]:nullptr), int1);
    }
  }

  friend std::istream& operator>>(std::istream& is, HDS& hds)
  {
    std::vector<Halfedge_handle> halfedges;
    std::vector<Vertex_handle> vertices;
    hds.load(is, halfedges, vertices);
    return is;
  }

  void export_to_xfig(Export_xfig& export_xfig,
                      double dx, double dy,
                      uint8_t coul_segments,
                      uint8_t coul_disks,
                      uint16_t width_segments,
                      uint16_t radius_disks) const
  {
    if (width_segments>0)
    {
      for (auto it=halfedges().begin(), itend=halfedges().end(); it!=itend; ++it)
      {
        if (it->vertex()!=nullptr && it->opposite()->vertex()!=nullptr &&
            it<it->opposite())
        {
          export_xfig.segment(CGAL::to_double(it->vertex()->point().x())+dx,
                              CGAL::to_double(it->vertex()->point().y())+dy,
                              CGAL::to_double(it->opposite()->vertex()->point().x())+dx,
                              CGAL::to_double(it->opposite()->vertex()->point().y())+dy,
                              coul_segments, width_segments, false, 90);
        }
      }
    }

    if (radius_disks>0)
    {
      for (auto it=vertices().begin(), itend=vertices().end(); it!=itend; ++it)
      {
        export_xfig.point(CGAL::to_double(it->point().x())+dx,
                          CGAL::to_double(it->point().y())+dy,
                          radius_disks, coul_disks, false, 70);
      }
    }
  }

protected:
    CGAL::Compact_container<Halfedge> m_halfedges;
    CGAL::Compact_container<Vertex>   m_vertices;
};
////////////////////////////////////////////////////////////////////////////////
#endif // HDS_H
