// Copyright (c) 2006,2007,2009,2010,2011 Tel-Aviv University (Israel).
// All rights reserved.
//
// This file is part of CGAL (www.cgal.org).
//
// $URL$
// $Id$
// SPDX-License-Identifier: GPL-3.0-or-later OR LicenseRef-Commercial
//
// Author(s)     : Tali Zvi <talizvi@post.tau.ac.il>
//                 Baruch Zukerman <baruchzu@post.tau.ac.il>
//                 Efi Fogel <efif@post.tau.ac.il>

#ifndef CGAL_MY_EVENT_H
#define CGAL_MY_EVENT_H

/*! \file
 *
 * Definition of the Arr_construction_event_base class-template.
 */

#include <vector>

#include <CGAL/Surface_sweep_2/Default_event_base.h>
#include <CGAL/assertions.h>
#include "Hds.h"
#include "My_construction_subcurve.h"

/*! \class Arr_construction_event_base
 *
 * This template represents an event used by the surface-sweep framework.  It
 * inherits either from `Default_event_base` (the default) or
 * 'No_overlap_event_base' depending on whether the curve may overlap or not.
 * It stores the data associated with an event in addition to the information
 * stored in its base class. When constructing an arrangement, additional
 * information is stored, in order to expedite the insertion of curves into the
 * arrangement.
 *
 * The additional infomation contains the following:
 * - among the left curves of the event, we keep the highest halfedge that
 *   was inserted into the arrangement at any given time and when there are no
 *   left curves, we keep the highest halfedge that was inseted to the right.
 *
 * \tparam GeometryTraits_2 the geometry traits.
 * \tparam Allocator_ a type of an element that is used to acquire/release
 *                    memory for elements of the event queue and the status
 *                    structure, and to construct/destroy the elements in that
 *                    memory. The type must meet the requirements of Allocator.
 * \tparam SurfaceSweepEvent a template, an instance of which is used as the
 *                           base class.
 *
 * \sa `Default_event_base`
 * \sa `No_overlap_event_base`
 */
  template <typename GeometryTraits_2, typename Subcurve_,
          template <typename, typename>
          class SurfaceSweepEvent = CGAL::Surface_sweep_2::Default_event_base>
class My_event_base :
  public SurfaceSweepEvent<GeometryTraits_2, Subcurve_>
{
public:
  typedef GeometryTraits_2                              Geometry_traits_2;
  typedef Subcurve_                                     Subcurve;

private:
  typedef Geometry_traits_2                               Gt2;
  typedef SurfaceSweepEvent<Gt2, Subcurve>                Base;
  typedef My_event_base<Gt2, Subcurve, SurfaceSweepEvent> Self;

public:
  typedef typename Gt2::X_monotone_curve_2              X_monotone_curve_2;
  typedef typename Gt2::Point_2                         Point_2;

  typedef typename Base::Subcurve_container        Subcurve_container;
  typedef typename Base::Subcurve_iterator         Subcurve_iterator;
  typedef typename Base::Subcurve_reverse_iterator Subcurve_reverse_iterator;

protected:
  Vertex_handle   m_vertex;         // The vertex handle associated with this event.
  Halfedge_handle m_above_halfedge; // The halfedge handle of the subcurve just above this event.
  bool            m_before_strip;
public:
  /*! Default constructor. */
  My_event_base(): m_vertex(nullptr), m_above_halfedge(nullptr), m_before_strip(false)
  {}

  /*! Destructor */
  ~My_event_base() {}

  /*! Set the vertex handle. */
  void set_vertex_handle(Vertex_handle vh) { m_vertex=vh; }

  /*! Get the vertex handle. */
  Vertex_handle vertex_handle() const { return m_vertex; }

  void set_above_halfedge(Halfedge_handle hh)
  { m_above_halfedge=hh; }
  Halfedge_handle above_halfedge() const
  { return m_above_halfedge; }

  bool before_strip() const
  { return m_before_strip; }
  void set_before_strip()
  { m_before_strip=true; }
};

template <typename GeometryTraits_2,
          typename Allocator_ = CGAL_ALLOCATOR(int)>
class My_event :
    public My_event_base<GeometryTraits_2,
                         My_construction_subcurve<GeometryTraits_2,
                                                  My_event<GeometryTraits_2, Allocator_>,
                                                  Allocator_> >
{
public:
  /*! Construct default. */
  My_event() {}
};

#endif
